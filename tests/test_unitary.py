import os
import shutil
import pandas as pd
import numpy as np
import pytest

from covidcolombia.insreport import Report
from covidcolombia.region import Region
from covidcolombia.summary import Summary
from covidcolombia.utils import colombia_demo_distribution, korea_cfr
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, DB_FIS_DATE

date = None
path = os.path.abspath(os.path.dirname(__file__))
dane_path = os.path.join(path, "../data/distribuciones_regiones")


# Clean up if necessary
try:
    shutil.rmtree(os.path.join(path, "tmp"))
except:
    pass


def test_download_from_ins():
    global date
    report = Report(os.path.join(path, "tmp"))
    date = report.date


def test_colombia_demo_distribution():
    path_to_file = os.path.join(path, "tmp", NAME_DEMO_DIST_COL)
    colombia_demo_distribution(dane_path, path_to_file, 3.25247)


def test_korea_cfr():
    path_to_file = os.path.join(path, "tmp", NAME_CFR_KOR)
    korea_cfr(path_to_file)


@pytest.fixture
def report():
    report = Report(
        os.path.join(path, "tmp"), date=date, region="BOGOTA", age_range=(5, 50)
    )
    return report


def test_load_database(report):
    _ = report.load_cases_database(nrows=1000)
    _ = report.load_cases_database(
        nrows=1000, cols=["Edad", "fecha reporte web"]
    )
    _ = report.load_tests_database()


def test_get_SIR_populations(report):
    day = pd.to_datetime(date) - pd.Timedelta("10 days")
    truncation_types = ["fecha reporte web", "FIS_all"]
    modes = ["incidence", "cummulative"]
    for truncation_type in truncation_types:
        for mode in modes:
            report.get_SIR_populations(day, None, truncation_type, mode)

    # Check that sum over 'incidence' adds up to 'cummulative':
    df_dates = report.load_cases_database(
        nrows=np.random.randint(1000, 3000),
        cols=["Fecha de notificación", DB_FIS_DATE, "fecha reporte web"],
    )[["Fecha de notificación", DB_FIS_DATE, "fecha reporte web"]]

    max_date = np.nanmax(df_dates.to_numpy())
    min_date = np.nanmin(df_dates.to_numpy())

    date_range = pd.date_range(min_date, max_date)

    muertos = []
    recuperados = []
    infectados = []

    for day in date_range:
        m, r, i = report.get_SIR_populations(
            day, None, "fecha reporte web", "incidence"
        )
        muertos.append(m)
        recuperados.append(r)
        infectados.append(i)

    muertos_incidence = sum(muertos)
    recuperados_incidence = sum(recuperados)
    infectados_incidence = sum(infectados)

    m, r, i = report.get_SIR_populations(
        date_range.max(), None, "fecha reporte web", "cummulative"
    )

    assert m == muertos_incidence
    assert r == recuperados_incidence
    assert i == infectados_incidence


def test_get_daily_incidence(report):
    day = pd.to_datetime(date) - pd.Timedelta("10 days")
    truncation_types = ["fecha reporte web", "FIS_all"]
    for truncation_type in truncation_types:
        report.get_daily_incidence(day, truncation_type)


def test_delay_distribution(report):
    report.delay_distribution(label1="Fecha de muerte", label2="FIS_all", truncate=True)
    report.delay_distribution(
        label1="Fecha de muerte", label2="FIS_all", truncate=False
    )


def test_fit_distribution(report):
    report.fit_distribution(dist="scipy:gamma", kwargs={})
    report.fit_distribution(
        dist="scipy:weibull", kwargs={"floc": 0, "f0": 1}, returnData=True
    )
    report.fit_distribution(dist="pymc3:gamma", kwargs={"samples": 50}, returnData=True)
    report.fit_distribution(dist="pymc3:lognormal", kwargs={"samples": 50})
    report.fit_distribution(dist="pymc3:weibull", kwargs={"samples": 50})
    report.fit_distribution(dist="pymc3:gumbel", kwargs={"samples": 50})
    report.fit_distribution(dist="pymc3:chi2", kwargs={"samples": 50})
    report.fit_distribution(dist="pymc3:wald", kwargs={"samples": 50})
    report.fit_distribution(dist="pymc3:inverse gamma", kwargs={"samples": 50})
    report.fit_distribution(dist="kde", returnData=True)
    report.fit_distribution(dist="kde")
    
    


def test_correct_CFR(report):
    day = pd.to_datetime(date) - pd.Timedelta("20 days")

    f = report.fit_distribution(dist="scipy:gamma", kwargs={})
    c1 = report.get_daily_incidence(
        day, "fecha reporte web"
    ).incidence.to_numpy()  # Truncated
    c2 = report.get_daily_incidence(
        None, "fecha reporte web"
    ).incidence.to_numpy()  # Not truncated
    cfr1, _, _, d1, _ = report.correct_CFR(
        day, f, c1, truncation_type="fecha reporte web", truncation_day=day
    )
    cfr2, _, _, d2, _ = report.correct_CFR(
        day, f, c2, truncation_type="fecha reporte web", truncation_day=None
    )

    # cfr2 should be smaller than cfr1 because there is
    # more data for cfr2. However, due to delay in report of deaths,
    # data truncation yield more reported deaths corresponding
    # to cfr2 than cfr1, making cfr2 > cfr1 possible. We can, however,
    # rescale cfr2 to be on the same basis as cfr1:
    assert cfr2/d2 * d1 < cfr1

    f = report.fit_distribution(dist="pymc3:gamma", kwargs={"samples": 50})
    cfr1, _, _, d1, _ = report.correct_CFR(
        day, f, c1, truncation_type="fecha reporte web", truncation_day=day
    )
    cfr2, _, _, d2, _ = report.correct_CFR(
        day, f, c2, truncation_type="fecha reporte web", truncation_day=None
    )

    # cfr2 should be smaller than cfr1 because there is
    # more data for cfr2.
    assert cfr2/d2 * d1 < cfr1


@pytest.fixture
def region1():
    col_demo_dist_path = os.path.join(path, "tmp", NAME_DEMO_DIST_COL)
    cfr_korea_path = os.path.join(path, "tmp", NAME_CFR_KOR)
    region = Region(os.path.join(path, "tmp"), col_demo_dist_path, cfr_korea_path)
    return region


@pytest.fixture
def region2():
    col_demo_dist_path = os.path.join(path, "tmp", NAME_DEMO_DIST_COL)
    cfr_korea_path = os.path.join(path, "tmp", NAME_CFR_KOR)
    region = Region(
        os.path.join(path, "tmp"),
        col_demo_dist_path,
        cfr_korea_path,
        region="MEDELLIN",
        date=date,
    )
    return region


def test_load_col_demo_dist(region1, region2):
    region1.load_col_demo_dist()
    region2.load_col_demo_dist()


def test_load_cfr_korea(region1, region2):
    region1.load_cfr_korea()
    region2.load_cfr_korea()


def test_compute_reporting_percentage(region1, region2):
    day = pd.to_datetime("2020-08-01")
    region1.compute_reporting_percentage(day, "fecha reporte web", day)
    region2.compute_reporting_percentage(day, "fecha reporte web", None)
    region2.compute_reporting_percentage(day, "FIS_all", day)
    region2.compute_reporting_percentage(day, "FIS_all", None)


def test_reporting_percentage_evolution(region2):
    region2.reporting_percentage_evolution("2020-07-20", "2020-07-25", timestep=2, mode="ignorant")
    region2.reporting_percentage_evolution("2020-07-20", "2020-07-25", timestep=3, mode="wise")
    d, _, _ = region2.reports["(0-MAX_AGE)"].get_SIR_populations(
        "2020-07-28", mode="cummulative"
    )
    assert d > 0
    region2.reporting_percentage_evolution(
        "2020-07-20", "2020-07-25", mode="ignorant", min_deaths=d
    )

@pytest.fixture
def summary():
    col_demo_dist_path = os.path.join(path, "tmp", NAME_DEMO_DIST_COL)
    cfr_korea_path = os.path.join(path, "tmp", NAME_CFR_KOR)
    summary = Summary(
        os.path.join(path, "tmp"),
        date,
        col_demo_dist_path,
        cfr_korea_path,
        regions=["VALLE", "ANTIOQUIA", "AMAZONAS"],
    )
    return summary


def test_summary_rep_perc_evolution(region2, summary):
    summary.reporting_percentage_evolution("2020-07-23", "2020-07-25", mode="ignorant")
    d, _, _ = region2.reports["(0-MAX_AGE)"].get_SIR_populations(
        "2020-07-24", mode="cummulative"
    )
    assert d > 0
    summary.reporting_percentage_evolution(
        "2020-07-20", "2020-07-25", mode="ignorant", min_deaths=d
    )

def test_summarise(summary):
    summary.summarise(date)

def test_get_curves(region2):
    region2.get_curves("2020-07-25", earliest_day="2020-07-20")


def test_remove_files():
    shutil.rmtree(os.path.join(path, "tmp"))
