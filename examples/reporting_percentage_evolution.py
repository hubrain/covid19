import os
from covidcolombia.utils import colombia_demo_distribution
from covidcolombia.utils import korea_cfr, agerange2string
from covidcolombia import Region
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, AGE_RANGES
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import time

path = os.path.abspath(os.path.dirname(__file__))
dane_path = os.path.join(path, "../data/distribuciones_regiones")

# We will save the INS files in examples/
col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
colombia_demo_distribution(dane_path, col_demo_path, 3.0)
korea_cfr(korea_cfr_path)

# Initialise Region
today = datetime.date.today()
init_day = "2020-03-15"
colombia = Region(path, col_demo_path, korea_cfr_path, region="Colombia", date=str(today))

# Perform the prophet method
t0 = time.time()
rep_percentages = colombia.reporting_percentage_evolution(init_day, today, mode='prophet', min_deaths=5)
rango = agerange2string(AGE_RANGES[-1])
print(f'prophet lasted {time.time() - t0:.2f}')
subdf = rep_percentages[rep_percentages.rango == rango]
urep = subdf.urep
inf = subdf.urep_inf
sup = subdf.urep_sup
x = subdf.day
plt.fill_between(x, inf, sup, color='green', alpha=0.4, label='prophet')

# Perform the wise method
t0 = time.time()
rep_percentages = colombia.reporting_percentage_evolution(init_day, today, mode='wise', min_deaths=5)
rango = agerange2string(AGE_RANGES[-1])
print(f'wise lasted {time.time() - t0:.2f}')
subdf = rep_percentages[rep_percentages.rango == rango]
urep = subdf.urep
inf = subdf.urep_inf
sup = subdf.urep_sup
x = subdf.day
plt.fill_between(x, inf, sup, color='blue', alpha=0.4, label='wise')

# Perform the ignorant method
t0 = time.time()
rep_percentages = colombia.reporting_percentage_evolution(init_day, today, mode='ignorant', min_deaths=5)
print(f'ignorant lasted {time.time() - t0:.2f}')
subdf = rep_percentages[rep_percentages.rango == rango]
urep = subdf.urep
inf = subdf.urep_inf
sup = subdf.urep_sup
x = subdf.day
plt.fill_between(x, inf, sup, color='red', alpha=0.4, label='ignorant')

# Plot and save
plt.legend()
plt.savefig('evolution_col.png', dpi=500)
plt.show()