"""
This example shows how to get incidence and prevalence curves
useful for epidemiological modelling.
"""

import os
import numpy as np
import datetime
from covidcolombia.utils import colombia_demo_distribution, korea_cfr, agerange2string
from covidcolombia import Region, Report
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, AGE_RANGES
import matplotlib.pyplot as plt

path = os.path.abspath(os.path.dirname(__file__))

path = os.path.abspath(os.path.dirname('.'))
dane_path = os.path.join(path, "../data/distribuciones_regiones")

# We will save the INS files in examples/
col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
colombia_demo_distribution(dane_path, col_demo_path, 3.0)
korea_cfr(korea_cfr_path)

# Get the delay_distribution of Colombia for all ages
today = datetime.date.today()
report = Report(path, date=str(today))
ft = report.fit_distribution()
ft = np.mean(ft, axis=0)

# Initialise Region
colombia = Region(path, col_demo_path, korea_cfr_path, region="Antioquia", date=str(today), delay_distribution=ft)
df = colombia.get_curves(str(today))

# Pick the age range that covers all ages:
df_all = df[df.rango == agerange2string(AGE_RANGES[-1])]

# Plot, for instance, the incidence of deaths:
plt.plot(df_all.date, df_all.death_incidence, 'k-')
plt.title('Death incidence')
plt.show()

# Plot the corrected cummulative cases curve:
plt.fill_between(df_all.date, df_all.corrected_cummulative_cases_inf, df_all.corrected_cummulative_cases_sup, color='blue', alpha=0.4)
plt.plot(df_all.date, df_all.corrected_cummulative_cases, 'b-', lw=2)
plt.title('Corrected number of cummulative cases')
plt.show()

# Plot the daily incidence vs the corrected daily incidence:
plt.plot(df_all.date, df_all.case_incidence, 'r-', label='Incidence')
plt.plot(df_all.date, df_all.corrected_cummulative_cases.diff(), 'b-', label='Corrected Incidence')
plt.title('Daily vs corrected daily incidence')
plt.legend()
plt.show()

# IMPORTANT: This is only valid up to some days in the past, because there is always a shortage
# of incidence in the last few days. If you want to consider them, try to use
# colombia.get_curves(str(today), method='prophet'), though results remain a bit uncertain!