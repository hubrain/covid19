import os
from covidcolombia.insreport import Report
import pandas as pd
import matplotlib.pyplot as plt

path = os.path.abspath(os.path.dirname(__file__))
path = '.'

report = Report(path, region="Colombia", age_range=(0, "MAX_AGE"), date="2020-12-22")
# dist = "pymc3:rice"
# delay_distribution = report.fit_distribution(dist=dist, kwargs={"samples":1000})
# params, data = report.fit_distribution(dist=dist, kwargs={"samples":1000}, returnData=True)

# plt.hist(data[data <= 100], bins=100, density=True)
# for line in delay_distribution:
#     plt.plot(line, c='green', alpha=0.1)
# plt.show()

dist = "kde"
delay_distribution = report.fit_distribution(dist=dist)
params, data = report.fit_distribution(dist=dist, returnData=True)

plt.hist(data[data <= 100], bins=100, density=True, alpha=0.5)
plt.plot(delay_distribution, c='green')
plt.show()