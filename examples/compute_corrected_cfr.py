import os
from covidcolombia.insreport import Report
import pandas as pd

path = os.path.abspath(os.path.dirname(__file__))

report = Report(path, date="2020-09-25", region="Colombia", age_range=(0, "MAX_AGE"))
delay_distribution = report.fit_distribution(dist="pymc3:lognormal", kwargs={"samples":1000})
daily_incidence = report.get_daily_incidence().incidence.to_numpy()

cfr, inf, sup, deaths = report.correct_CFR(pd.to_datetime("2020-09-25"), delay_distribution, daily_incidence)

# Total cases
cases = report.load_cases_database(cols=["Edad"]).shape[0]

print(cfr, inf, sup, 100*deaths/cases)