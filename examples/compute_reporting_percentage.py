import os
from covidcolombia.utils import colombia_demo_distribution
from covidcolombia.utils import korea_cfr
from covidcolombia.region import Region
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR
import datetime
import pandas as pd

path = os.path.abspath(os.path.dirname(__file__))
dane_path = os.path.join(path, "../data/distribuciones_regiones")

# We will save the INS files in examples/
col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
colombia_demo_distribution(dane_path, col_demo_path, 3.0)
korea_cfr(korea_cfr_path)
today = datetime.date.today()
valledelcauca = Region(path, col_demo_path, korea_cfr_path, region="Valle del Cauca", date=str(today))
print(valledelcauca.compute_reporting_percentage(pd.to_datetime(today)))
