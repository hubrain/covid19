URL_INS_CASES = "https://www.datos.gov.co/api/views/gt2j-8ykr/rows.csv"
URL_INS_TESTS = "https://www.datos.gov.co/resource/8835-5baf.csv"
NAME_CASES_DB = "casosINS.csv"
NAME_TESTS_DB = "testsINS.csv"
NAME_DEMO_DIST_COL = "distribucion_pobla.csv"
NAME_CFR_KOR = "cfr_korea.csv"
AGE_RANGES = [
    (0, 39),
    (40, 49),
    (50, 59),
    (60, 69),
    (70, 79),
    (80, "MAX_AGE"),
    (0, "MAX_AGE"),
]
EVOLUTION_NAME = "evolution.csv"
SUMMARY_NAME = "summary.csv"
INS_DEPTO_TO_TEST = {
    "AMAZONAS": "amazonas",
    "ANTIOQUIA": "antioquia",
    "ARAUCA": "arauca",
    "SAN ANDRES": "san_andres",
    "ATLANTICO": "atlantico",
    "BARRANQUILLA": "barranquilla",
    "BOGOTA": "bogota",
    "BOLIVAR": "bolivar",
    "BOYACA": "boyaca",
    "BUENAVENTURA": "valle_del_cauca",
    "CALDAS": "caldas",
    "CAQUETA": "caqueta",
    "CARTAGENA": "cartagena",
    "CASANARE": "casanare",
    "CAUCA": "cauca",
    "CESAR": "cesar",
    "CHOCO": "choco",
    "CORDOBA": "cordoba",
    "CUNDINAMARCA": "cundinamarca",
    "GUAINIA": "guainia",
    "GUAVIARE": "guaviare",
    "HUILA": "huila",
    "GUAJIRA": "guajira",
    "MAGDALENA": "magdalena",
    "META": "meta",
    "NARIÑO": "narino",
    "NORTE SANTANDER": "norte_de_santander",
    "PUTUMAYO": "putumayo",
    "QUINDIO": "quindio",
    "RISARALDA": "risaralda",
    "STA MARTA D.E.": "santa_marta",
    "SANTANDER": "santander",
    "SUCRE": "sucre",
    "TOLIMA": "tolima",
    "VALLE": "valle_del_cauca",
    "VAUPES": "vaupes",
    "VICHADA": "vichada",
}
DB_DEPTO = "Nombre departamento"
DB_MUNICIPIO = "Nombre municipio"
DB_RECU_DATE = "Fecha de recuperación"
DB_FIS_DATE = "Fecha de inicio de síntomas"
