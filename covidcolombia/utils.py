import os
import numpy as np
import pandas as pd
from statsmodels.stats.proportion import proportion_confint
from scipy.special import gamma as Gamma
from scipy.special import iv as I0
from covidcolombia.constants import AGE_RANGES
from scipy.stats import chi2
from scipy.optimize import toms748


def log_gamma_pdf(x, α, β):
    return α * np.log(β) + (α - 1) * np.log(x) - β * x - np.log(Gamma(α))


def log_weibull_pdf(x, α, β):
    return np.log(α) + (α - 1) * np.log(x) - (x / β) ** α - α * np.log(β)


def log_lognormal_pdf(x, μ, σ):
    return (
        - np.log(x)
        - np.log(σ * np.sqrt(2 * np.pi))
        - (np.log(x) - μ) ** 2 / (2 * σ ** 2)
    )

def log_chi2_pdf(x, ν):
    return 0.5*(ν - 2) * np.log(x) - 0.5 * x - (0.5 * ν * np.log(2) + np.log(Gamma(0.5 * ν)))

def log_wald_pdf(x, μ, λ):
    return 0.5 * λ/(2*np.pi) + -3/2 * x - λ/(2*x) * ((x-μ)/μ)**2

def log_inv_gamma_pdf(x, α, β):
    return α * np.log(β) - np.log(Gamma(α)) - (α + 1) * x - β/x

def log_gumbel_pdf(x, μ, β):
    z = (x - μ) / β
    return -(z + np.exp(-z)) - np.log(β)

def log_rice_pdf(x, ν, σ):
    return np.log(x) - 2 * np.log(σ) - (x*x + ν*ν)/(2*σ*σ) + np.log(I0(x * ν / (σ*σ)))


def list_end_suffix(suffix, folder):
    """
    Function that finds every filename within a folder ending on a suffix.

    Parameters
    ----------
    suffix: str
        Extension of a file, e.g. `csv`
    folder: str
        Absolute path

    Returns
    -------
    List[str]
        List of absolute paths to the files in the folder ending with a suffix.
    """
    filenames = os.listdir(folder)
    return [
        os.path.join(folder, filename)
        for filename in filenames
        if filename.endswith(suffix)
    ]


def concat_csv(folder):
    """
    Concatenates all csv files (assuming they share headers) found within a folder.

    Parameters
    ----------
    folder: str
        Absolute path

    Returns
    -------
    pandas.DataFrame
        A dataframe where all csvs are concatenated.
    """
    filenames = list_end_suffix(".csv", folder)
    dfs = [pd.read_csv(filename) for filename in filenames]

    # Check that headers are the same:
    for i, df in enumerate(dfs[1:]):
        if not np.array_equal(df.columns, dfs[0].columns):
            raise AttributeError(
                f"""
            Columns of file {filenames[i]} are {df.columns},
            which are different from file {filenames[0]}, which are {df[0].columns}.
            """
            )

    return pd.concat(dfs)


def parsing_string_regiones(region):
    """
    Consistency between DANE and INS requires to change the name of some cities.

    Parameters
    ----------
    region: str
        The name of a region as given by DANE.

    Returns
    -------
    str
        The name of a region as given by INS.
    """
    if region == "Bogotá. D. C.":
        return "BOGOTA"
    elif region == "Caldas":
        return "CALDAS"
    elif region == "Boyacá":
        return "BOYACA"
    elif region == "Tolima":
        return "TOLIMA"
    elif region == "Cundinamarca":
        return "CUNDINAMARCA"
    elif region == "Caquetá":
        return "CAQUETA"
    elif region == "Putumayo":
        return "PUTUMAYO"
    elif region == "Córdoba":
        return "CORDOBA"
    elif region == "Atlántico":
        return "ATLANTICO"
    elif region == "Bolívar":
        return "BOLIVAR"
    elif region == "Valle del Cauca":
        return "VALLE"
    elif region == "Vichada":
        return "VICHADA"
    elif region == "Cauca":
        return "CAUCA"
    elif region == "Arauca":
        return "ARAUCA"
    elif region == "Chocó":
        return "CHOCO"
    elif region == "Guaviare":
        return "GUAVIARE"
    elif region == "Casanare":
        return "CASANARE"
    elif region == "La Guajira":
        return "GUAJIRA"
    elif region == "Cesar":
        return "CESAR"
    elif region == "Magdalena":
        return "MAGDALENA"
    elif region == "Meta":
        return "META"
    elif region == "Risaralda":
        return "RISARALDA"
    elif region == "Antioquia":
        return "ANTIOQUIA"
    elif region == "Sucre":
        return "SUCRE"
    elif region == "Huila":
        return "HUILA"
    elif region == "Santander":
        return "SANTANDER"
    elif region == "Amazonas":
        return "AMAZONAS"
    elif region == "Nariño":
        return "NARIÑO"
    elif region == "Cartagena":
        return "CARTAGENA"
    elif region == "Barranquilla":
        return "BARRANQUILLA"
    elif region == "N. Santander":
        return "NORTE SANTANDER"
    elif region == "Buenaventura":
        return "BUENAVENTURA"
    elif region == "Santa Marta":
        return "STA MARTA D.E."
    elif region == "Nacional":
        return "Colombia"
    elif region == "Quindio":
        return "QUINDIO"
    elif region == "San Andrés":
        return "SAN ANDRES"
    elif region == "Vaupes":
        return "VAUPES"
    elif region == "Guaínia":
        return "GUAINIA"
    else:
        return region


def truncate_df(df, threshold, column):
    """
    Truncate an INS dataframe if a column **exceeds** a threshold.

    Parameters
    ----------
    df: pd.DataFrame
        A dataframe that contains a column named `column`.
    threshold: float or datetime.datetime
        A threshold value. Data above this value will be dropped.
    column: str
        The name of a column where the truncation will occur.
        This can be 'fecha reporte web' or 'FIS_all'.

    Returns
    -------
    pd.DataFrame
        The truncated dataframe
    """
    if column == "fecha reporte web" or column == "FIS_all":
        truncated_df = df[df[column] <= threshold]
    else:
        raise ValueError(
            f"""
            column {column} is not supported.
            It should be 'fecha reporte web' or 'FIS_all'.
            """
        )

    return truncated_df


def agerange2string(agerange):
    return f"({'-'.join([str(item) for item in agerange])})"


def colombia_demo_distribution(dane_distribution_folder, path_to_file, perc_threshold):
    """
    Computes the age distribution for all regions in the country.
    Creates a dataframe containing this age distribution and saves
    it to some given folder.

    Parameters
    ----------
    dane_distribution_folder: str
        Absolute path to a directory containing the age distributions from DANE.
        These are to be found at https://sitios.dane.gov.co/cnpv/#!/
    path_to_file: str
        Absolute path to the file where the age distributions will be saved.
    perc_threshold: float
        Percentage that distributions from DANE are allowed to be off, when added.
        They should add up to X, where |X - 100%| < `perc_threshold`.
    """
    df = concat_csv(dane_distribution_folder)
    df = df[["departamento", "EDAD", "HOMBRES_2018", "MUJERES_2018"]]
    regiones = df["departamento"].unique()
    source = {"departamento": [], "rango": [], "dist": []}

    for region in regiones:
        # Primer rango de edad
        df_region = df[df["departamento"] == region]
        mask_1 = (
            (df_region["EDAD"] == "0-4")
            | (df_region["EDAD"] == "5-9")
            | (df_region["EDAD"] == "10-14")
            | (df_region["EDAD"] == "15-19")
            | (df_region["EDAD"] == "20-24")
            | (df_region["EDAD"] == "25-29")
            | (df_region["EDAD"] == "30-34")
            | (df_region["EDAD"] == "35-39")
        )
        rango_1 = agerange2string(AGE_RANGES[0])

        # Segundo rango de edadd
        mask_2 = (df_region["EDAD"] == "40-44") | (df_region["EDAD"] == "45-49")
        rango_2 = agerange2string(AGE_RANGES[1])

        # Tercer rango de edad
        mask_3 = (df_region["EDAD"] == "50-54") | (df_region["EDAD"] == "55-59")
        rango_3 = agerange2string(AGE_RANGES[2])

        # Cuarto rango de edad
        mask_4 = (df_region["EDAD"] == "60-64") | (df_region["EDAD"] == "65-69")
        rango_4 = agerange2string(AGE_RANGES[3])

        # Quinto rango de edad
        mask_5 = (df_region["EDAD"] == "70-74") | (df_region["EDAD"] == "75-79")
        rango_5 = agerange2string(AGE_RANGES[4])

        # Sexto rango de edad
        mask_6 = (
            (df_region["EDAD"] == "80-84")
            | (df_region["EDAD"] == "85-89")
            | (df_region["EDAD"] == "90-94")
            | (df_region["EDAD"] == "95-99")
            | (df_region["EDAD"] == "100+")
        )
        rango_6 = agerange2string(AGE_RANGES[5])
        masks = [mask_1, mask_2, mask_3, mask_4, mask_5, mask_6]
        rangos = [rango_1, rango_2, rango_3, rango_4, rango_5, rango_6]
        for mask, rango in zip(masks, rangos):
            df_rango_edad = df_region[mask]
            population_rango_edad = (
                df_rango_edad[["HOMBRES_2018", "MUJERES_2018"]].to_numpy().sum()
            )
            assert isinstance(population_rango_edad, float)
            source["departamento"].append(parsing_string_regiones(region))
            source["rango"].append(rango)
            source["dist"].append(population_rango_edad)

    df_distribution = pd.DataFrame(source)

    # Some regions were re-named
    new_regiones = df_distribution.departamento.unique()

    for region in new_regiones:
        # Check that distribution adds to 100%
        total = df_distribution[df_distribution.departamento == region]["dist"].sum()
        assert isinstance(total, float)
        if abs(total - 100) > perc_threshold:
            raise Exception(
                f"""
            Region {region} distribution sum added up to {total},
            which is far from 100 by an amount larger than the threshold {perc_threshold}.
            """
            )
        # Normalise the distribution
        df_distribution.loc[df_distribution.departamento == region, "dist"] = (
            df_distribution[df_distribution.departamento == region].dist
            / (df_distribution[df_distribution.departamento == region].dist.sum())
            * 100
        )
    df_distribution.to_csv(path_to_file, index=False)


def korea_cfr(path_to_file):
    """
    Function that creates a csv containing the the age-segregated CFR
    for the Republic of Korea.

    Parameters
    ----------
    path_to_file: str
        Absolute path to the file where the age distributions will be saved.
    """
    ranges = AGE_RANGES[:-1]

    cfr_korea = [0.12, 0.17, 0.62, 2.32, 9.31, 24.96]
    muertos = [2, 3, 15, 41, 84, 144]
    casos = [round(m / cfr * 100) for cfr, m in zip(cfr_korea, muertos)]
    casos[0] += 10874 - 7106
    cfr_korea[0] = muertos[0] / casos[0] * 100

    infs = []
    sups = []

    for i in range(len(ranges)):
        inf, sup = np.array(proportion_confint(muertos[i], casos[i])) * 100
        infs.append(inf)
        sups.append(sup)

    cfrkorea = pd.DataFrame(
        dict(
            rango=[agerange2string(agerange) for agerange in ranges],
            cfr=cfr_korea,
            sup=sups,
            inf=infs,
            casos=casos,
            muertos=muertos,
        )
    )
    cfrkorea.to_csv(os.path.join(path_to_file))


def pad(t, f, c):
    """
    Adds a padding to f and c, which are the onset-to-death delay
    distribution and daily incidence.

    Parameters
    ----------
    t: int
        Number of days from patient zero to a predefined day.
    f: numpy.array
        Onset-to-death delay distribution.
        The shape is (N,), where N is the number of days taken
        into account to sample the onset-to-death distribution.
    c: numpy.array
        Daily incidence **from the day of patient zero**.
        The shape is (N,), where N is the number of days
        from patient zero to a predefined day. In general, N
        can be different from t.

    Returns
    -------
    numpy.array
        padded onset-to-death delay distribution
    numpy.array
        padded daily incidence distribution
    """
    if f.size < t:
        newf = np.pad(f, (0, t - f.size), "constant", constant_values=(0, 0))
    elif f.size > t:
        newf = f[:t]
    else:
        newf = f
    if c.size < t:
        newc = np.pad(c, (0, t - c.size), "constant", constant_values=(0, 0))
    elif c.size > t:
        newc = c[:t]
    else:
        newc = c
    return newf, newc


def X_over_Y_koopman(X_sample_size, Y_sample_size, X_success, Y_success):
    """
    Computes X/Y for two random variables X, Y ~ Binomial with sample parameters
    given by a `sample_size` and a maximum likelihood probability given by the
    number of `success` obtained from the `sample_size`.

    Parameters
    ----------
    X_sample_size: int
        Sample size of the random variable X.
    Y_sample_size: int
        Sample size of the random variable Y.
    X_success: int
        Number of observed successes from random variable X
        (in our case, number of deaths).
    Y_success: int
        Number of observed successes from random variable Y
        (in our case, number of deaths).

    Returns
    -------
    float
        Nominal value of X/Y (in percentage).
    float
        Inferior bound with 95% confidence (in percentage).
    float
        Superior bound with 95% confidence (in percentage).
    """

    # Definición de funciones (doi: 10.2307/2531405)
    def p1(θ, n1=X_sample_size, y=Y_success, n2=Y_sample_size, x=X_success):
        numerator = (
            θ * (n1 + y)
            + x
            + n2
            - np.sqrt((θ * (n1 + y) + x + n2) ** 2 - 4 * θ * (n1 + n2) * (x + y))
        )
        return numerator / (2 * (n1 + n2))

    # def p2(θ, n1=X_sample_size, y=Y_success, n2=Y_sample_size, x=X_success):
    #     return p1(θ, n1, y, n2, x) / θ

    def X2(θ, n1=X_sample_size, y=Y_success, n2=Y_sample_size, x=X_success):
        p1_eval = p1(θ, n1, y, n2, x)
        first_term = (x - n1 * p1_eval) ** 2 / (n1 * p1_eval * (1 - p1_eval))
        second_term = 1 + n1 * (θ - p1_eval) / (n2 * (1 - p1_eval))
        return first_term * second_term

    X21 = chi2.ppf(0.95, 1)  # 1 degree of freedom, 95% confidence interval

    ratio = (X_success / X_sample_size) / (Y_success / Y_sample_size)
    try:
        if ratio >= 1:
            inf = toms748(lambda x: X2(x) - X21, 1e-10, ratio)
            if inf >= 1:
                inf = 1
            ratio = 1
            sup = 1
        else:
            inf = toms748(lambda x: X2(x) - X21, 1e-10, ratio)
            if X2(ratio) - X21 < 0 and X2(1 - 1e-10) - X21 > 0:
                sup = toms748(lambda x: X2(x) - X21, ratio, 1 - 1e-10)
            else:
                sup = 1
    except ValueError as e:
        print(e)
        raise ValueError(
            f"Raised with X_ss(korea)={X_sample_size}, Y_ss(region)={Y_sample_size}, X_success(korea)={X_success}, Y_success(region)={Y_success}"
        )

    return ratio * 100, inf * 100, sup * 100


class MinMaxScaler:
    """"""

    # TODO: write documentation
    def __init__(self, a=0, b=1):
        """"""
        # TODO: write documentation
        # New variable min and max
        self.lo = a
        self.hi = b
        # Old variable min and max
        self.min = None
        self.max = None
        # Transformation variables
        self.m = None
        self.c = None

    def fit(self, X):
        """"""
        # TODO: write documentation
        self.min = X.min()
        self.max = X.max()
        self.m = (self.hi - self.lo) / (self.max - self.min)
        self.c = -self.min * self.m + self.lo # pylint: disable=invalid-unary-operand-type

    def transform(self, X):
        """"""
        # TODO: write documentation
        if self.m is None or self.c is None:
            raise TypeError("Please run .fit before transforming.")
        return self.m * X + self.c

    def fit_transform(self, X):
        """"""
        # TODO: write documentation
        self.fit(X)
        return self.transform(X)

    def inverse_transform(self, X):
        """"""
        # TODO: write documentation
        if self.m is None or self.c is None:
            raise TypeError("Please run .fit before transforming.")
        return (X - self.c) / self.m
