from .constants import (
    NAME_CASES_DB,
    NAME_TESTS_DB,
    URL_INS_TESTS,
    URL_INS_CASES,
    INS_DEPTO_TO_TEST,
    DB_DEPTO,
    DB_RECU_DATE,
    DB_FIS_DATE,
    DB_MUNICIPIO,
)
from .utils import (
    log_lognormal_pdf,
    log_gamma_pdf,
    log_weibull_pdf,
    log_chi2_pdf,
    log_wald_pdf,
    log_inv_gamma_pdf,
    log_gumbel_pdf,
    log_rice_pdf,
    truncate_df,
    pad,
    MinMaxScaler,
)

import pandas as pd
import numpy as np
import os
import datetime
from scipy.integrate import quad
import pymc3 as pm
from pandas.api.types import is_datetime64_dtype
from scipy import stats
import wget
import warnings
from statsmodels.stats.proportion import proportion_confint
import matplotlib.pyplot as plt


class Report:
    """
    `Report` is the main class of this project.

    It enables handling the INS report for any arbitrary subset of records
    from the INS COVID-19 database from Datos Abiertos.

    Attributes
    ----------
    region: str or None
        Name of the region.
    datapath: str
        Absolute path to a directory containing the date folders with paths
        "datapath/YYYY-MM-DD".
    age_range: List[int] or None
        An age range for the `Report`.
    date: str or None
        If it's a string, the format must be "YYYY-MM-DD". It refers to the name
        of a folder that contains information about a specfic date.
    downloaded_ins_cases: bool
        Whether or not the INS cases have been downloaded at datapath/date.
    downloaded_ins_tests: bool
        Whether or not the INS tests have been downloaded at datapath/date.
    largest_db: pd.DataFrame
        A dataframe that contains the largest DB so far used in memory for quick computations.

    Methods
    -------
    request_ins_csv_files()
        Download cases and tests datasets from INS to `datapath/date`.
    load_cases_database(nrows, cols, fis_all)
        Loads the database of cases corresponding to the date `date`.
    load_tests_database()
        Loads the database of tests corresponding to the date `date`.
    predict_daily_incidence(truncation_day, truncation_type, alpha, noise, extend, verbose)
        Predicts the daily incidence (with respect to "fecha reporte web") of the future
        `extend` days. Then, it propagates the onset-to-report distribution to accommodate
        those new predicted cases into the past. The prediction is done by fitting a line
        to the data of the last 30 days y = m*x + b.
    get_SIR_populations(day, truncation_day, truncation_type, mode)
        Calculates the number of deaths, recoveries and infections at a given `day`.
        The data is truncated up to a `day`.
    get_daily_incidence(truncation_day, truncation_type)
        Computes the daily incidence based on onset symptoms date.
        Assumes asymptomatic cases "onset symptoms date" is the
        notification date.
    delay_distribution(label1, label2, truncate)
        Compute the exact delay distribution between dates in the two
        provided labels. The most wanted delay distribution is onset date
        to death date. Asymptomatic cases (who don't die) can be included,
        as this does not modify the output of this distribution.
    fit_distribution(label1, label2, truncate, dist, kwargs, Xdays, probs_mode, returnData)
        Function to model the onset-to-death delay distribution.
    correct_CFR(day, f, c, truncation_type, truncation_day, mode)
        This function corrects the CFR accounting for the outcome delay for a given day.
    """

    # TODO: ensure that every method that receives dates can do it using strings

    def __init__(self, datapath, date=None, region=None, age_range=None):
        """
        Constructs a new `Report` to handle any arbitrary subset of INS data.

        Given a date, all methods will load data from the folder that contains
        information for this date. Otherwise, it will create a folder that will
        contain information for the current date.

        Parameters
        ----------
        datapath: str
            An absolute path to a directory containing the date folders with paths
            "datapath/YYYY-MM-DD".
        date: str or None, optional
            If it's a string, the format must be "YYYY-MM-DD". If it is `None`,
            today's date is used. Default is `None`.
        region: str or None, optional
            Region to load, if `"Colombia"` or `None`, the whole dataset is used.
            Default is `None`.
        age_range: List[int] or None, optional
            Age range to load. If `None`, the whole age range is used.
            The format must be `[min_age, max_age]`. Default is `None`.
        """
        if date is None:
            date = str(datetime.date.today())
        exists = os.path.exists(os.path.join(datapath, date))

        if region == "Colombia":
            self.region = None
        else:
            self.region = region

        self.depto = None

        self.datapath = datapath
        self.age_range = age_range
        self.date = date
        self.largest_db = pd.DataFrame()

        if exists:
            # check if files exist
            if os.path.exists(os.path.join(datapath, date, NAME_CASES_DB)):
                self.downloaded_ins_cases = True
            else:
                self.downloaded_ins_cases = False
            if os.path.exists(os.path.join(datapath, date, NAME_TESTS_DB)):
                self.downloaded_ins_tests = True
            else:
                self.downloaded_ins_tests = False
        else:
            self.downloaded_ins_cases = False
            self.downloaded_ins_tests = False

        if not self.downloaded_ins_cases or not self.downloaded_ins_tests:
            self.request_ins_csv_files()

    def request_ins_csv_files(self):
        """
        Download cases and tests datasets from INS to `self.datapath/self.date`.
        """
        path_to_data = os.path.join(self.datapath, self.date)
        try:
            try:
                os.mkdir(self.datapath)
            except:
                print(self.datapath, "already exists")
            os.mkdir(path_to_data)
        except:
            print(path_to_data, "already exists")

        # Download cases:
        if not os.path.exists(os.path.join(path_to_data, NAME_CASES_DB)):
            wget.download(URL_INS_CASES, out=os.path.join(path_to_data, NAME_CASES_DB))

        # Download tests:
        if not os.path.exists(os.path.join(path_to_data, NAME_TESTS_DB)):
            df_tests = pd.read_csv(URL_INS_TESTS).iloc[1:]
            df_tests.to_csv(
                os.path.join(path_to_data, NAME_TESTS_DB), sep=",", index=False
            )

        self.downloaded_ins_cases = True
        self.downloaded_ins_tests = True

    def load_cases_database(self, nrows=None, cols=None, fis_all=False):
        """
        Loads the database of cases corresponding to the date `self.date`.

        Parameters
        ----------
        nrows: int or None, optional
            Number of rows from the database to be loaded in memory.
            If `None`, it loads all the available rows to memory.
        cols: List[str] or None, optional
            A list containing the names of the columns to be loaded
            in memory.
        fis_all: bool, optional
            Whether or not to create a new column "FIS_all" which contains the
            onset symptoms date for symptomatic cases, or the notification
            date for asymptomatic cases. This also creates a new column
            "Fecha recuperado_all", where we assume that asymptomatic cases
            recover after 15 days.

        Returns
        -------
        pd.DataFrame
            DataFrame containing cases records.

        Notes
        -----
        "fecha reporte web" is the date where each record was first published
        on the web. However, due to data correction or data errors, sometimes
        other dates are modified for some records, causing that other dates
        such as onset dates, diagnostic dates or notification dates (which)
        are shown as larger than the reporting date. In principle, these
        dates should be earlier than the reporting date (or update date).
        Therefore, in the case of "fecha reporte web" being in `cols`, we will
        detect the cases where other dates (onset, diagnostic and notification)
        are also in `cols`. Then, we will set for each record
        "fecha reporte web" to be the maximum out of this date and the others.
        """
        if cols is None:
            cols_prime = []
        else:
            cols_prime = cols.copy()
        if fis_all:
            cols_prime += ["FIS_all"]
        if set(cols_prime).issubset(set(self.largest_db.columns)):
            if nrows is not None:
                if nrows <= self.largest_db.shape[0]:
                    return self.largest_db.iloc[:nrows][cols_prime]
            else:
                if self.largest_db.shape[0] != 0:
                    return self.largest_db[cols_prime]

        path_to_cases = os.path.join(self.datapath, self.date, NAME_CASES_DB)

        if fis_all:
            if cols is None:
                cols = []
            if DB_FIS_DATE not in cols:
                cols += [DB_FIS_DATE]
            if DB_RECU_DATE not in cols:
                cols += [DB_RECU_DATE]
            if "Fecha de notificación" not in cols:
                cols += ["Fecha de notificación"]

        if self.age_range is not None:
            if cols is None:
                cols = []
            cols += ["Edad"]

        if self.region is not None:
            if cols is None:
                cols = []
            cols += [DB_DEPTO, DB_MUNICIPIO]

        df_cases = pd.read_csv(path_to_cases, nrows=nrows, usecols=set(cols))

        if self.region is not None:
            regions = (
                df_cases[DB_DEPTO].unique().tolist()
                + df_cases[DB_MUNICIPIO].unique().tolist()
            )
            mask_depto = df_cases[DB_DEPTO] == self.region
            mask_mun = df_cases[DB_MUNICIPIO] == self.region
            if mask_depto.sum() == 0:
                if mask_mun.sum() == 0:
                    raise ValueError(
                        f"""
                                    Region {self.region} is not defined in the INS database.
                                    Check a correct name. Delete this object and create a new one
                                    with a correct region. You can choose from the following:
                                    {regions}
                                    """
                    )
                else:
                    df_cases = df_cases[mask_mun]
            else:
                df_cases = df_cases[mask_depto]

            if self.depto is None:
                self.depto = df_cases[DB_DEPTO].unique()[0]

        if self.age_range is not None:
            lower_condition = df_cases["Edad"] >= self.age_range[0]
            if self.age_range[1] == "MAX_AGE":
                maxage = df_cases["Edad"].max()
            else:
                maxage = self.age_range[1]
            upper_condition = df_cases["Edad"] <= maxage
            df_cases = df_cases[lower_condition & upper_condition]

        time_columns = [
            "Fecha diagnostico",
            DB_FIS_DATE,
            "Fecha de muerte",
            DB_RECU_DATE,
            "Fecha de notificación",
            "fecha reporte web",
        ]

        to_be_filled = [
            "Fecha diagnostico",
            DB_FIS_DATE,
            "Fecha de notificación",
            "fecha reporte web",
        ]

        if cols is None:
            cols = df_cases.columns

        for col in time_columns:
            if col in cols:
                assert col in df_cases.columns
                newcol = pd.to_datetime(
                    df_cases[col], errors="coerce", dayfirst=True
                ).dt.date
                newcol = pd.to_datetime(newcol)
                if col in to_be_filled:
                    df_cases[col] = newcol.ffill()
                else:
                    df_cases[col] = newcol

        if fis_all:
            mask = df_cases[DB_FIS_DATE] == "Asintomático"
            fis_col = np.empty(df_cases.shape[0])
            fis_col[:] = np.nan
            fis_col = pd.Series(
                data=fis_col, dtype=df_cases[DB_FIS_DATE].dtype, index=df_cases.index
            )
            fis_col[~mask] = df_cases[~mask][DB_FIS_DATE]
            fis_col[mask] = df_cases[mask]["Fecha de notificación"]
            df_cases["FIS_all"] = fis_col

            rec_col = np.empty(df_cases.shape[0])
            rec_col[:] = np.nan
            rec_col = pd.Series(
                data=rec_col,
                dtype=df_cases[DB_RECU_DATE].dtype,
                index=df_cases.index,
            )
            rec_col[~mask] = df_cases[~mask][DB_RECU_DATE]
            rec_col[mask] = df_cases[mask]["FIS_all"] + pd.Timedelta("15 days")
            df_cases[DB_RECU_DATE] = rec_col

        if "fecha reporte web" in cols:
            dates_columns = set(df_cases.columns).intersection(
                set(to_be_filled + ["FIS_all"])
            )
            max_dates = df_cases[dates_columns].max(axis=1)
            df_cases["fecha reporte web"] = max_dates

        self.largest_db = df_cases
        return df_cases

    def load_tests_database(self):
        """
        Loads the database of tests corresponding to the date `self.date`.
        The method only retrieves information of `self.depto`.

        Returns
        -------
        pd.DataFrame
            DataFrame containing tests records.
        """

        path_to_tests = os.path.join(self.datapath, self.date, NAME_TESTS_DB)
        df_tests = pd.read_csv(path_to_tests)

        assert "fecha" in df_tests.columns
        df_tests["fecha"] = pd.to_datetime(df_tests["fecha"], errors="coerce")

        if self.depto is not None:
            if self.depto == "Buenaventura D.E.":
                warnings.warn(
                    "Buenaventura D.E. is not reported in the tests database",
                    UserWarning,
                )
            region_name = INS_DEPTO_TO_TEST[self.depto]
            assert region_name in df_tests.columns
        else:
            region_name = "acumuladas"
        df_tests = df_tests[["fecha", region_name]]
        df_tests = df_tests[~df_tests[region_name].isna()]
        df_tests.loc[:, region_name] = df_tests[region_name].diff()
        # The first record is probably wrong because it contains
        # accumulated tests, and not the tests performed that day
        return df_tests.iloc[1:]

    def predict_daily_incidence(
        self,
        truncation_day=None,
        truncation_type="fecha reporte web",
        alpha=0.1,
        noise=0.5,
        extend=70,
        verbose=False,
    ):
        """
        Predicts the daily incidence (with respect to "fecha reporte web") of the future
        `extend` days. Then, it propagates the onset-to-report distribution to accommodate
        those new predicted cases into the past. The prediction is done by fitting a line
        to the data of the last 30 days y = m*x + b.

        Parameters
        ----------
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        truncation_type: str
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS_all' to truncate given the onset symptoms
            date.
        alpha: float
            The slope of the fit is modifyied by multiplying by alpha.
        noise: float
            Positive real number. The predictions are generated by a line that is
            smeared out by generating normally distributed noise with an amplitude
            (standard deviation) given by `noise` times the standard deviation
            of the data around the fit.
        extend: int
            Positive integer denoting the number of days to predict into the future
        verbose: bool
            If `True`, the method will plot the data and the prediction.

        Returns
        -------
        pd.DataFrame
            DataFrame with 2 columns: 'fecha' and 'FIS_all'. It contains the incidence
            (as given by onset date) for every day at 'fecha' that must be added to the
            true reported incidence at 'fecha', i.e. these are the predicted cases,
            propagated to the past through the onset-to-report distribution.

        """

        cols2load = ["fecha reporte web"]
        if truncation_type == "FIS_all":
            fis_all = True
        else:
            fis_all = False
        df_cases = self.load_cases_database(cols=cols2load, fis_all=fis_all)
        if truncation_day is None:
            truncation_day = df_cases["fecha reporte web"].max()
        df_truncated = truncate_df(df_cases, truncation_day, truncation_type)
        date_range = pd.date_range(
            df_truncated["fecha reporte web"].min(), truncation_day
        )
        date_reporte = df_truncated["fecha reporte web"]

        # Compute the reporting incidence time series
        incidence = []
        for day in date_range:
            num_infected = (date_reporte == day).sum()
            incidence.append(num_infected)
        incidence = np.array(incidence)
        df_incidence = pd.DataFrame(dict(fecha=date_range, incidence=incidence))

        # Transform variable ranges
        dates_int = np.array([date.value for date in date_range])
        dt = (date_range.max() - date_range.min()).days + 1
        date_scaler = MinMaxScaler(0, dt - 1)
        incidence_scaler = MinMaxScaler()
        dates_scaled = date_scaler.fit_transform(dates_int)
        incidence_scaled = incidence_scaler.fit_transform(incidence)
        dates_new = np.arange(dt, dt + extend)

        # Linear Regression
        if df_incidence.shape[0] >= 30:
            slope, intercept, _r_value, _p_value, _std_err = stats.linregress(
                dates_scaled[-30:], incidence_scaled[-30:]
            )
        else:
            slope = 0
            intercept = np.mean(incidence_scaled)

        # Modify regression
        linear = lambda x, m, b: m * x + b
        x0 = dates_scaled[-1]
        y0 = linear(x0, slope, intercept)
        cut = min(30, dates_scaled.size)
        error = incidence_scaled[-cut:] - linear(dates_scaled[-cut:], slope, intercept)
        sd = np.sqrt(np.mean(np.abs(error) ** 2)) * noise
        slope *= alpha
        intercept = y0 - slope * x0

        # Predict
        predicted_incidence = linear(
            dates_new, slope, intercept
        ) + sd * np.random.randn(extend)
        predicted_incidence = np.max(
            np.vstack((predicted_incidence, np.zeros(extend))), axis=0
        )
        predicted_incidence = np.round(
            incidence_scaler.inverse_transform(predicted_incidence)
        )
        dates_new = pd.to_datetime(date_scaler.inverse_transform(dates_new))
        df_prediction = pd.DataFrame(
            dict(fecha=dates_new, incidence=predicted_incidence)
        )

        # Propagate onset-to-report distribution through incidence
        if self.depto is not None:
            warnings.warn(
                f"Considering onset-to-report distribution only for district {self.depto}",
                UserWarning,
            )
        delay = self.delay_distribution(label1="fecha reporte web", label2="FIS_all")
        delay = delay[delay <= extend]
        day_range = range(delay.min(), delay.max() + 1)
        prob_delay = np.array([(delay == day).sum() for day in day_range])
        t = min(extend, prob_delay.size)
        padded_delay, padded_incidence = pad(t, prob_delay, predicted_incidence)
        padded_delay = padded_delay / padded_delay.sum()
        new_FIS = [
            np.round(np.dot(padded_delay[-i:], padded_incidence[:i]))
            for i in range(1, t + 1)
        ]
        dates_new_FIS = pd.date_range(
            df_prediction.fecha.min() - pd.Timedelta(f"{t} days"),
            df_prediction.fecha.min() - pd.Timedelta("1 days"),
        )

        df_new_FIS = pd.DataFrame(dict(fecha=dates_new_FIS, FIS_all=new_FIS))

        if verbose:
            _fig, axes = plt.subplots(nrows=2, figsize=(15, 14))
            axes[0].plot(df_incidence.fecha, df_incidence.incidence, "ko", label="data")
            axes[0].plot(
                df_prediction.fecha, df_prediction.incidence, "ro", label="prediction"
            )
            axes[1].plot(df_new_FIS.fecha, df_new_FIS.FIS_all, "k-")
            plt.legend()
            plt.show()

        return df_new_FIS

    def get_SIR_populations(
        self,
        day,
        truncation_day=None,
        truncation_type="fecha reporte web",
        mode="incidence",
    ):
        """
        Calculates the number of deaths, recoveries and infections at a given day.
        The data is truncated up to a day

        Parameters
        ----------
        day: str or datetime64
            The day in which to calculate the number of deaths, recoveries
            and infections in format "YYYY-MM-DD".
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        truncation_type: str
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS_all' to truncate given the onset symptoms
            date.
        mode: str
            Can be 'incidence' or 'cummulative'. If 'incidence', cases at
            a day are reported. If 'cummulative', cases up to a day are reported.

        Returns
        -------
        float
            Number of deaths at a day if `mode` is 'incidence', or up to
            a day if `mode` is 'cummulative'.
        float
            Number of recoveries at a day if `mode` is 'incidence', or up to
            a day if `mode` is 'cummulative'.
        float
            Number of infections at a day if `mode` is 'incidence', or up to
            a day if `mode` is 'cummulative'.

        Notes
        -----
        When we calculate the number of infections, we **do not** calculate the
        **prevalence**, but the **incidence**. Since we also have asymptomatic
        cases, for the moment we assume that their onset symptoms date is the
        same notification date, and that they "recover" 15 days after.
        """
        if isinstance(day, str):
            day = pd.to_datetime(day)

        cols2load = [
            DB_FIS_DATE,
            "Fecha de notificación",
            "Fecha de muerte",
            DB_RECU_DATE,
            "fecha reporte web",
        ]
        df_cases = self.load_cases_database(cols=cols2load, fis_all=True)
        if truncation_day is None:
            df_truncated = df_cases
        else:
            df_truncated = truncate_df(df_cases, truncation_day, truncation_type)
        mask = df_truncated[DB_FIS_DATE] == "Asintomático"
        date_fis = df_truncated["FIS_all"]
        date_asympt = df_truncated[mask]["Fecha de notificación"]

        if mode == "incidence":
            num_deaths = (df_truncated["Fecha de muerte"] == day).sum()
            num_recovered = (df_truncated[DB_RECU_DATE] == day).sum()
            num_recovered += (date_asympt + pd.Timedelta("15 days") == day).sum()
            num_infected = (date_fis == day).sum()
        elif mode == "cummulative":
            num_deaths = (df_truncated["Fecha de muerte"] <= day).sum()
            num_recovered = (df_truncated[DB_RECU_DATE] <= day).sum()
            num_recovered += (date_asympt + pd.Timedelta("15 days") <= day).sum()
            num_infected = (date_fis <= day).sum()
        else:
            raise ValueError(
                f"""
                mode {mode} is not allowed. Only 'incidence' and
                'cummulative' are supported.
                """
            )

        return num_deaths, num_recovered, num_infected

    def get_daily_incidence(
        self, truncation_day=None, truncation_type="fecha reporte web"
    ):
        """
        Computes the daily incidence based on onset symptoms date.
        Assumes asymptomatic cases "onset symptoms date" is the
        notification date.

        Parameters
        ----------
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        truncation_type: str
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS_all' to truncate given the onset symptoms
            date.

        Returns
        -------
        pd.DataFrame
            Two column dataframe: date and its corresponding incidence.
        """
        cols2load = ["fecha reporte web"]
        df_cases = self.load_cases_database(cols=cols2load, fis_all=True)
        if truncation_day is None:
            truncation_day = df_cases["fecha reporte web"].max()
        df_truncated = truncate_df(df_cases, truncation_day, truncation_type)
        try:
            date_range = pd.date_range(df_truncated["FIS_all"].min(), truncation_day)
        except ValueError:
            raise ValueError(
                f"Either start ({df_truncated['FIS_all'].min()}) or end ({truncation_day}) is NaT"
            )
        date_fis = df_truncated["FIS_all"]
        incidence = []
        for day in date_range:
            num_infected = (date_fis == day).sum()
            incidence.append(num_infected)

        return pd.DataFrame(dict(date=date_range, incidence=incidence))

    def delay_distribution(
        self, label1="Fecha de muerte", label2="FIS_all", truncate=True
    ):
        """
        Compute the exact delay distribution between dates in the two
        provided labels. The most wanted delay distribution is onset date
        to death date. Asymptomatic cases (who don't die) can be included,
        as this does not modify the output of this distribution.

        Parameters
        ----------
        label1: str, optional
            A column referring to a date. Default is "Fecha de muerte".
        label2: str, optional
            A column referring to a date. Default is "FIS_all".
        truncate: bool, optional
            Whether to truncate or not the data so that the last 60 days
            are not taken into account. This is important to avoid using
            the latest data because it favours fast-resolving cases,
            damaging the sample.

        Returns
        -------
        np.array
            An array containing the delay from date in label1
            to date in label2.
        """
        # TODO: just for functionality, we might want to include
        # another type of truncation: 1) truncate the db to set
        # a level of ignorance, 2) the truncation that is already
        # implemented to avoid fast-resolving cases.

        cols = list(set((label1, label2, "fecha reporte web")))
        if "FIS_all" in cols:
            fis_all = True
            cols.remove("FIS_all")
        else:
            fis_all = False
        df_cases = self.load_cases_database(cols=cols, fis_all=fis_all)
        last_day = df_cases["fecha reporte web"].max()
        if truncate:
            df_truncated = truncate_df(
                df_cases, last_day - pd.Timedelta("100 days"), "FIS_all"
            )
        else:
            df_truncated = df_cases
        days_delay = (df_truncated[label1] - df_truncated[label2]).dt.days
        days_delay = days_delay[~days_delay.isna()].to_numpy()
        return days_delay[days_delay > 0]

    def _get_probs_from_traces(self, log_pdf, traces, Xdays, dt, probs_mode="integrate"):
        """
        Gets distribution from traces obtained through pymc3

        Parameters
        ----------
        log_pdf: callable
            Logarithm of a probability density funciton.
        traces: tuple
            Tuple of arrays of the same length. Each component of each array
            is a sample, and each array corresponds to a parameter.
        Xdays: List[float]
            A list like range(number of days) at which the distribution is to be sampled.
        dt: float
            A timestep.    
        probs_mode: str
            A mode for sampling the probabilities out of the distribution.
            'integrate' integrates the pdf around each sampling date.
            'direct' samples directly the pdf at each sampling date.
        """
        ft = []
        for trace in zip(*traces):
            pdf_func = lambda x: np.exp(log_pdf(x, *trace))
            if probs_mode == "integrate":
                probs = [
                    quad(pdf_func, max(-dt + i, 1e-6), dt + i)[0] for i in Xdays
                ]
            elif probs_mode == "direct":
                probs = [pdf_func(i) for i in Xdays]
            else:
                raise ValueError(
                    f"probability mode {probs_mode} is not supported."
                )
            ft.append(np.asarray(probs) / np.sum(probs))

        return np.asarray(ft)

    def fit_distribution(
        self,
        label1="Fecha de muerte",
        label2="FIS_all",
        truncate=True,
        dist="pymc3:lognormal",
        kwargs={"samples": 1000},
        Xdays=range(100),
        probs_mode="integrate",
        returnData=False,
    ):
        """
        Function to model the onset-to-death delay distribution.

        Parameters
        ----------
        label1: str
            Date column to be comparted with label2
        label2: str
            Date column to be compared with label1
        truncate: bool
            Tells if data should be truncated to not take into account results from the last 100 days
        dist: str
            A string specifying which distribution to use. Currently supported are:

                - 'kde' for a kernel density estimation
                - 'scipy:gamma' for a gamma distribution given by the scipy package
                - 'scipy:weibull' for a Weibull distribution given by the scipy package
                - 'pymc3:gamma' for a gamma distribution given by the pymc3 package
                - 'pymc3:lognormal' for a lognormal distribution given by the pymc3 package
                - 'pymc3:weibull' for a Weibull distribution given by the pymc3 package
                - 'pymc3:chi2' for a Chi squared distribution given by the pymc3 package
                - 'pymc3:wald' for a Wald distribution given by the pymc3 package
                - 'pymc3:inverse gamma' for an InverseGamma distribution given by the pymc3 package
                - 'pymc3:gumbel' for a Gumbel distribution given by the pymc3 package
        kwargs: dictionary
            Keyword arguments to be passed to the dist when fitting (e.g. for scipy:weibull kwargs={'floc':0, 'f0':1},
            or for pymc3:gamma kwargs={'samples': 1000})
        Xdays: List[float]
            A list like range(number of days) at which the distribution is to be sampled.
        probs_mode: str
            A mode for sampling the probabilities out of the distribution.
            'integrate' integrates the pdf around each sampling date.
            'direct' samples directly the pdf at each sampling date.
        returnData bool
            Whether or not to return the data from the real distribution

        Returns
        -------
        numpy.array
            (Xdays.size,) where the ith component is the probability that a person dies after i days of his/her onset
            for a scipy distribution.
            or (kwargs['samples'], Xdays.size) where each row is the ith component of the probability that a person
            dies after i days of this/her onset for a pymc3 distribution.
            **This is returned only if returnData=False**

        tuple
            if scipy is used, params of a scipy distribution, and the actual measured distribution of onset-to-death.
            if pymc3 is used, mean fitted data, and the actual measured distribution of onset-to-death.
            **This is returned only if returnData=True**.

        Raises
        ------
        ValueError
            If `dist` is not supported.
        """
        if self.region is not None and self.region != "Colombia":
            warnings.warn(
                f"A distribution is gonna be fit, but region {self.region} is not Colombia",
                UserWarning,
            )
        if ":" in dist:
            dist_package, dist_name = dist.split(":")
        elif dist == "kde":
            dist_package = "kde"
            dist_name = dist_package
        else:
            raise ValueError(f"{dist} is not a valid distribution. See documentaiton")

        days = self.delay_distribution(label1=label1, label2=label2, truncate=truncate)
        dt = (Xdays[1] - Xdays[0]) * 0.5
        if dist == 'kde':
            kernel = stats.gaussian_kde(days)
            params = None
            if probs_mode == "integrate":
                probs = [
                    quad(kernel, max(-dt + i, 1e-6), dt + i)[0]
                    for i in Xdays
                ]
            elif probs_mode == "direct":
                probs = [kernel(i) for i in Xdays]
            else:
                raise ValueError(f"probability mode {probs_mode} is not supported.")
            ft = np.asarray(probs) / np.sum(probs)
            if returnData:
                return params, days
        elif dist_package == "scipy":
            if dist_name == "gamma":
                distribution = stats.gamma
            elif dist_name == "weibull":
                distribution = stats.exponweib
            else:
                raise ValueError(
                    f"{dist_name} is not supported for {dist_package} in our module yet. See documentation"
                )
            params = distribution.fit(days, **kwargs)
            if probs_mode == "integrate":
                probs = [
                    quad(distribution.pdf, max(-dt + i, 1e-6), dt + i, args=params)[0]
                    for i in Xdays
                ]
            elif probs_mode == "direct":
                probs = [distribution.pdf(i) for i in Xdays]
            else:
                raise ValueError(f"probability mode {probs_mode} is not supported.")
            ft = np.asarray(probs) / np.sum(probs)
            if returnData:
                return params, days
        elif dist_package == "pymc3":
            assert "samples" in kwargs
            model = pm.Model()
            if dist_name == "gamma":
                with model:
                    alpha = pm.Exponential("alpha", lam=10)
                    beta = pm.Exponential("beta", lam=0.1)
                    _g = pm.Gamma("g", alpha=alpha, beta=beta, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                alphas = trace.posterior["alpha"].data.flatten()
                betas = trace.posterior["beta"].data.flatten()
                params = {"alphas": alphas, "betas": betas}

                idx = np.random.choice(
                    alphas.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_gamma_pdf, (alphas[idx], betas[idx]), Xdays, dt, probs_mode)
            elif dist_name == "lognormal":
                with model:
                    mu = pm.HalfNormal("mu", 0.5)
                    sigma = pm.Exponential("sigma", lam=1.0)
                    _ln = pm.Lognormal("ln", mu=mu, sigma=sigma, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                mus = trace.posterior["mu"].data.flatten()
                sigmas = trace.posterior["sigma"].data.flatten()
                params = {"mus": mus, "sigmas": sigmas}

                idx = np.random.choice(mus.size, size=kwargs["samples"], replace=False)
                ft = self._get_probs_from_traces(log_lognormal_pdf, (mus[idx], sigmas[idx]), Xdays, dt, probs_mode)

            elif dist_name == "weibull":
                with model:
                    alpha = pm.Exponential("alpha", lam=1.5)
                    beta = pm.Exponential("beta", lam=1.0)
                    _wei = pm.Weibull("wei", alpha=alpha, beta=beta, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                alphas = trace.posterior["alpha"].data.flatten()
                betas = trace.posterior["beta"].data.flatten()
                params = {"alphas": alphas, "betas": betas}

                idx = np.random.choice(
                    alphas.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_weibull_pdf, (alphas[idx], betas[idx]), Xdays, dt, probs_mode)

            elif dist_name == "chi2":
                with model:
                    nu = pm.Exponential("nu", lam=1/6.0, testval=6.0)
                    _chi2 = pm.ChiSquared("chi2", nu=nu, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                nus = trace.posterior["nu"].data.flatten()
                params = {"nus": nus}

                idx = np.random.choice(
                    nus.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_chi2_pdf, (nus[idx],), Xdays, dt, probs_mode)

            elif dist_name == "wald":
                with model:
                    mu = pm.Exponential("mu", lam=1/15.0, testval=15.0)
                    lam = pm.Exponential("lambda", lam=1/340.0, testval=340.0)
                    _wald = pm.Wald("wald", mu=mu, lam=lam, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                mus = trace.posterior["mu"].data.flatten()
                lambdas = trace.posterior["lambda"].data.flatten()
                params = {"mus": mus, "lambdas": lambdas}

                idx = np.random.choice(
                    mus.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_wald_pdf, (mus[idx], lambdas[idx]), Xdays, dt, probs_mode)

            elif dist_name == "inverse gamma":
                with model:
                    alpha = pm.HalfNormal("alpha", 0.5) + 1
                    beta = pm.Exponential("beta", lam=1/0.5, testval=0.5)
                    _inv_gamma = pm.InverseGamma("inv_gamma", alpha=alpha, beta=beta, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                alphas = trace.posterior["alpha"].data.flatten() + 1
                betas = trace.posterior["beta"].data.flatten()
                params = {"alphas": alphas, "betas": betas}

                idx = np.random.choice(
                    alphas.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_inv_gamma_pdf, (alphas[idx], betas[idx]), Xdays, dt, probs_mode)

            elif dist_name == "gumbel":
                with model:
                    mu = pm.Normal("mu", testval=-1.0)
                    beta = pm.Exponential("beta", lam=1/4.0, testval=4.0)
                    _gumbel = pm.Gumbel("gumbel", mu=mu, beta=beta, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)

                mus = trace.posterior["mu"].data.flatten()
                betas = trace.posterior["beta"].data.flatten()
                params ={"mus": mus, "betas": betas}

                idx = np.random.choice(
                    mus.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_gumbel_pdf, (mus[idx], betas[idx]), Xdays, dt, probs_mode)

            elif dist_name == "rice":
                warnings.warn("pymc3:rice distribution is unstable. Use at your own risk. Probably will not fit", UserWarning)

                # FIXME: Rice is unstable... it is reaching a bad initial energy.
                with model:
                    #nu = pm.Exponential("nu", lam=1/0.1, testval=0.1)
                    sigma = pm.Gamma("sigma", alpha=2.0, beta=0.5)
                    _rice = pm.Rice("rice", nu=0, sigma=sigma, observed=days)
                    trace = pm.sample(kwargs["samples"], return_inferencedata=True)
                
                #nus = trace.posterior["nu"].data.flatten()
                sigmas = trace.posterior["sigma"].data.flatten()
                params = {"sigmas" : sigmas}

                idx = np.random.choice(
                    mus.size, size=kwargs["samples"], replace=False
                )
                ft = self._get_probs_from_traces(log_rice_pdf, (sigmas[idx],), Xdays, dt, probs_mode)
            else:
                raise ValueError(
                    f"{dist_name} is not supported for {dist_package} in our module yet. See documentation"
                )
            if returnData:
                return params, days
        return ft

    def correct_CFR(
        self,
        day,
        f,
        c,
        truncation_type="fecha reporte web",
        truncation_day=None,
        mode="ignorant",
    ):
        r"""
        This function corrects the CFR accounting for the outcome delay for a given day.

        Parameters
        ----------
        day: str or datetime64
            Day in which to calculate the corrected CFR.
        f: numpy.array
            Array with the onset-to-death delay distribution.
            The shape can be (N,) for some N, or can be (M, N), where N is the number of
            days taken into account (hopefully large) to sample the distribution.
            M is the number of different distributions sampled from the parameter distribution
            of the delay model.
        c: numpy.array
            Daily incidence (this should include both symptomatic and asymptomatic cases)
            Número de casos nuevos publicados cada día desde el paciente 0.
        truncation_type: str
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS' to truncate given the onset symptoms
            date.
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        mode: str, optional
            Can be either 'ignorant','wise' or 'prophet'. If 'ignorant',
            it will truncate the database at every day of the evolution
            range. If 'wise', it will not truncate. If 'prophet', truncates
            like 'ignorant', but predicts future cases to correct incidence.

        Returns
        -------
        float
            adjusted CFR (cCFR) in percentage.
        float
            inferior bound for cCFR (2.5%) in percentage.
        float
            superior bound for cCFR (97.5%) in percentage.
        int
            number of deaths up to `day`.
        int
            number of cases up to `day`.

        Notes
        -----
        The numberand by Nishiura is

        .. math::
            \sum_{i=0}^t\sum_{j=0}^\infty c_{i-j}f_j = c_0(f_0+\ldots+f_t)
            + c_1(f_0 + \ldots + f_{t-1}) + \ldots + c_t f_0

        which is equivalent to take the dot product between

        .. math:: (c_0,\ldots, c_t)

        and a vector whose :math:`i`-th component is (:math:`i` counts from 0)

        .. math:: f_0+\ldots+f_{t-i}

        which corresponds to a numpy cumsum.
        """
        if isinstance(day, str):
            day = pd.to_datetime(day)

        if mode == "prophet":
            df_new_incidence = self.predict_daily_incidence(
                truncation_day,
                truncation_type,
                alpha=0,
                noise=0.5,
                extend=70,
                verbose=False,
            )

        num_deaths, _num_recovered, num_infected = self.get_SIR_populations(
            day, truncation_day, truncation_type, mode="cummulative"
        )
        if mode == "prophet":
            # TODO: the following is a source of inconsistency.
            # I assume that the last day of c is the last day of
            # df_new_incidence
            length = min(df_new_incidence.shape[0], c.size)
            new_cases = df_new_incidence.FIS_all.to_numpy()[-length:]
            c[-length:] = c[-length:] + new_cases
            num_infected += np.sum(new_cases)

        df_cases = self.load_cases_database(fis_all=True)
        # Si no hay muertos registrados, el CFR se asume como 0.
        if num_deaths == 0 or num_infected == 0:
            assert num_infected >= num_deaths
            return 0, 0, 0, num_deaths, num_infected

        # Number of days from patient 0 (+1 because we count the initial day):
        t = (day - df_cases["FIS_all"].min()).days + 1

        if len(f.shape) == 1:
            padded_f, padded_c = pad(t, f, c)
            # numerator for u_t
            numerador = np.dot(padded_c, np.cumsum(padded_f)[::-1])
            # denominator for u_t
            denominador = np.sum(padded_c)

            # check that denominator (which sould be the total number
            # of infected people) is indeed the total number of
            # infected people:
            assert num_infected == denominador

            ut = numerador / denominador

            # Estimator of cCFR:
            pt = min([num_deaths / (ut * num_infected) * 100, 100])

            # Error bars with exact binomial method:
            success = num_deaths
            trials = ut * num_infected
            if success < trials:
                inf, sup = (
                    np.array(proportion_confint(success, trials, method="beta")) * 100
                )
            else:
                pt = 100
                inf, sup = (
                    np.array(proportion_confint(trials, trials, method="beta")) * 100
                )

        else:
            assert len(f.shape) == 2
            # If there are various distributions, we save all the estimations of the cCFR:
            pts = []
            for f_sample in f:
                padded_f, padded_c = pad(t, f_sample, c)
                numerador = np.dot(padded_c, np.cumsum(padded_f)[::-1])
                denominador = np.sum(padded_c)
                assert num_infected == denominador
                ut = numerador / denominador
                pt = min([num_deaths / (ut * num_infected) * 100, 100])
                pts.append(pt)
            pts = np.asarray(pts)
            inf = np.quantile(pts, 0.025)
            sup = np.quantile(pts, 0.975)
            pt = np.mean(pts)

        # Si algún número de estos resulta siendo NaN, entonces por defecto se retorna que el CFR es NaN:
        if inf != inf or sup != sup:
            return np.nan, np.nan, np.nan, num_deaths, num_infected
        return pt, inf, sup, num_deaths, num_infected
