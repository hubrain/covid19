from covidcolombia import Report
from covidcolombia import Region
from covidcolombia.constants import AGE_RANGES, EVOLUTION_NAME, SUMMARY_NAME, DB_DEPTO
from covidcolombia.utils import agerange2string

import os
import pandas as pd
import numpy as np


class Summary:
    """
    `Summary` is a class that facilitates the construction of summarising
    information at a given date. Files saved and built through this class'
    methods can be consumed for any dashboard-like reporter.

    Attributes
    ----------
    datapath: str
        An absolute path to a directory containing the date folders with paths
        "datapath/YYYY-MM-DD".
    date: str or None
        If it's a string, the format must be "YYYY-MM-DD".It refers to the name
        of a folder that contains information about a specfic date.
    report: covidcolombia.Report
        A Report object containing all the regions in Colombia.
    regions: List[str]
        A list of the regions taken into account in the summary.
    col_demo_dist_path: str
        Absolute path where the demographic distribution from Colombia and its
        regions was saved using `covidcolombia.utils.colombia_demo_distribution`.
    cfr_korea_path: str
        Absolute path where the CFR for the Republic of Korea was saved using
        `covidcolombia.utils.korea_cfr`.

    Methods
    -------
    reporting_percentage_evolution(initial_day, final_day, mode, min_deaths)
        Computes the evolution of the reporting percentage for all regions.
        It saves the resulting dataframe to the specified path:
        self.datapath/self.date/
    summarise(self, day, truncation_type, truncation_day)
        Computes the reporting percentage, cCFR, blCFR, nCFR, deaths and cases with
        the corresponding confidence intervals for every region and age range.
        It saves the resulting dataframe to the specified path:
        self.datapath/self.date/
    """

    def __init__(
        self, datapath, date, col_demo_dist_path, cfr_korea_path, regions=None
    ):
        """
        Constructs a new `Summary` object, which is useful to derive reporting
        percentages, nCFRs, cCFRs, blCFRs of every single region in the country.

        Parameters
        ----------
        datapath: str
            An absolute path to a directory containing the date folders with paths
            "datapath/YYYY-MM-DD".
        date: str or None, optional
            If it's a string, the format must be "YYYY-MM-DD".It refers to the name
            of a folder that contains information about a specfic date.
        col_demo_dist_path: str
            Absolute path where the demographic distribution from Colombia and its
            regions was saved using `covidcolombia.utils.colombia_demo_distribution`.
        cfr_korea_path: str
            Absolute path where the CFR for the Republic of Korea was saved using
            `covidcolombia.utils.korea_cfr`.
        regions: List[str] or None, optional
            A list containing the regions to be taken into account in the summary.
            If None, all available regions are to be downloaded.
        """
        self.datapath = datapath
        self.date = date
        self.report = Report(self.datapath, self.date)
        if regions is not None:
            self.regions = regions
        else:
            regions_counts = self.report.load_cases_database(cols=[DB_DEPTO])[
                DB_DEPTO
            ].value_counts()

            # Enforce that regions must have a minimum of records to be taken into account.
            # This is helpful to avoid typing errors from INS!
            self.regions = list(regions_counts[regions_counts > 100].index)
        if "Colombia" not in self.regions:
            self.regions.append("Colombia")
        self.col_demo_dist_path = col_demo_dist_path
        self.cfr_korea_path = cfr_korea_path

    def reporting_percentage_evolution(
        self, initial_day, final_day, timestep=1, mode="ignorant", min_deaths=None, dist='kde',
    ):
        """
        Computes the evolution of the reporting percentage for all regions.
        It saves the resulting dataframe to the specified path:
        self.datapath/self.date/

        Parameters
        ----------
        initial_day: str
            A date formated as a string: "YYYY-MM-DD".
        final_day: str
            A date formated as a string: "YYYY-MM-DD".
        timestep: int
            A timestep in days to report the evolution.
        mode: str, optional
            Can be either 'ignorant' or 'wise'. If 'ignorant', it will
            truncate the database at every day of the evolution range.
            If 'wise', it will not truncate.
        min_deaths: int or None, optional
            A number indicating the minimum number of deaths at the
            `initial_day`. If the number of deaths by the `initial_day`
            does not match `min_deaths`, then `initial_day` is overriden.
        dist: str, optional
            A distribution to pass to `Report.fit_distribution`
        Returns
        -------
        pd.DataFrame
            The columns are 'region' for each region in the country,
            (including Colombia) 'day' for every day between `initial_day`
            to the `final_day`, 'rango' for age ranges, 'urep' for computed
            reporting percentages, and 'urep_inf' and 'urep_sup' for inferior
            and superior bounds of reporting percentage.
        """

        evolutions = []
        colombia = Region(
            self.datapath,
            self.col_demo_dist_path,
            self.cfr_korea_path,
            region="Colombia",
            date=self.date,
        )
        delay_distribution = colombia.reports[
            agerange2string(AGE_RANGES[-1])
        ].fit_distribution(dist=dist)
        if delay_distribution.ndim == 2:
            delay_distribution = np.mean(delay_distribution, axis=0)
        del colombia
        for region_name in self.regions:
            region = Region(
                self.datapath,
                self.col_demo_dist_path,
                self.cfr_korea_path,
                region=region_name,
                date=self.date,
                delay_distribution=delay_distribution,
            )
            evolution = region.reporting_percentage_evolution(
                initial_day, final_day, timestep=timestep, mode=mode, min_deaths=min_deaths, dist=dist,
            )
            if evolution is not None:
                evolution.loc[:, "region"] = region_name
                evolutions.append(
                    evolution[
                        ["region", "day", "rango", "urep", "urep_inf", "urep_sup"]
                    ]
                )
            del region

        evolutions = pd.concat(evolutions)
        evolutions.to_csv(
            os.path.join(self.datapath, self.date, EVOLUTION_NAME), index=False
        )

        return evolutions

    def summarise(self, day, truncation_type="fecha reporte web", truncation_day=None, dist='kde'):
        """
        Computes the reporting percentage, cCFR, blCFR, nCFR, deaths and cases with
        the corresponding confidence intervals for every region and age range.
        It saves the resulting dataframe to the specified path:
        self.datapath/self.date/

        Parameters
        ----------
        day: str or datetime64
            Day in which to calculate the reporting percentage. If its a string,
            the format is "YYYY-MM-DD".
        truncation_type: str, optional
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS_all' to truncate given the onset symptoms
            date.
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        dist: str, optional
            A distribution to pass to `Report.fit_distribution`

        Returns
        -------
        pd.DataFrame
            The columns are 'region' for each region in the country,
            (including Colombia), 'rango' for age ranges, 'urep' for computed
            reporting percentages, 'urep_inf' and 'urep_sup' for inferior
            and superior bounds of reporting percentage, 'blCFR' for the
            baseline CFR, 'cCFR', 'cCFR_inf' and 'cCFR_sup' for the
            corrected CFR, 'deaths' for the number of deaths, 'cases' for
            the number of cases and 'nCFR' for the naïve CFR.
        """
        colombia = Region(
            self.datapath,
            self.col_demo_dist_path,
            self.cfr_korea_path,
            region="Colombia",
            date=self.date,
        )
        delay_distribution = colombia.reports[
            agerange2string(AGE_RANGES[-1])
        ].fit_distribution(dist=dist)
        print(f'Delay distribution has been computed with distribution {dist}')
        if delay_distribution.ndim == 2:
            delay_distribution = np.mean(delay_distribution, axis=0)
        del colombia

        region_summary = []
        for region_name in self.regions:
            region = Region(
                self.datapath,
                self.col_demo_dist_path,
                self.cfr_korea_path,
                region=region_name,
                date=self.date,
                delay_distribution=delay_distribution,
            )
            print(f"Region {region_name} has been created.")
            urep_cfr_df = region.compute_reporting_percentage(
                day, truncation_type, truncation_day, dist=dist
            )
            print("Reporting percentage for this region has been computed.")
            urep_cfr_df.loc[:, "region"] = region_name
            region_summary.append(urep_cfr_df)

        region_summary = pd.concat(region_summary)
        region_summary.to_csv(
            os.path.join(self.datapath, self.date, SUMMARY_NAME), index=False
        )

        return region_summary
