from covidcolombia.insreport import Report
from covidcolombia.constants import AGE_RANGES
from covidcolombia.utils import agerange2string, X_over_Y_koopman

import datetime
import os
import pandas as pd
import numpy as np


class Region:
    """
    `Region` is a class that uses the data from the INS database to pinpoint
    a specific region (or the whole country). This class facilitates the user
    in getting corrected CFRs and report percentages for each age range, namely:

    - 0-39 years
    - 40-49 years
    - 50-59 years
    - 60-69 years
    - 70-79 years
    - 80+ years
    - 0+ years (the whole dataset)

    Attributes
    ----------
    region: str or None
        Name of the region.
    datapath: str
        Absolute path to a directory containing the date folders with paths
        "datapath/YYYY-MM-DD".
    col_demo_dist_path: str
        Absolute path where the demographic distribution from Colombia and its
        regions was saved using `covidcolombia.utils.colombia_demo_distribution`.
    cfr_korea_path: str
        Absolute path where the CFR for the Republic of Korea was saved using
        `covidcolombia.utils.korea_cfr`.
    date: str or None
        If it's a string, the format must be "YYYY-MM-DD". It refers to the name
        of a folder that contains information about a specfic date.
    reports: dict[covidcolombia.insreport.Report]
        A dictionary containing `Report` objects for each age range.
    delay_distribution: List[float] or None
        A holder for the delay distribution.


    Methods
    -------
    load_col_demo_dist()
        Loads the demographic distribution from Colombia and its regions.
    load_cfr_korea()
        Loads the CFR for the Republic of Korea.
    compute_reporting_percentage(day, truncation_type, truncation_day, mode)
        Computes the reporting percentage at a given `day` for all age ranges of a
        region. Uses Koopman errors to estimate the confidence intervals of this
        reporting percentage estimation.
    reporting_percentage_evolution(initial_day, final_day, mode, min_deaths)
        Computes the evolution of reporting percentage from an `initial_day`
        to a `final_day`. There are different modes for doing this:

        - ignorant `mode`. To compute the rep. perc. at each day, data is
        truncated to that date.
        - wise `mode`. Data is not truncated, and all information from today's
        database is used.
        - prophet `mode`. Adopts the ignorant framework, but tries to predict
        the incidence from the coming days.

    get_curves(self, day, earliest_day, mode)
        Computes the following curves:

        - Daily deaths
        - Incidence
        - Prevalence
        - Corrected cummulative cases

        where the corrected cummulative cases curve accounts for underreporting.
    """

    def __init__(
        self,
        datapath,
        col_demo_dist_path,
        cfr_korea_path,
        region=None,
        date=None,
        delay_distribution=None,
    ):
        """
        Constructs a new `Region` object to handle INS data
        Constructs a new report object based on INS data.

        Given a date, all methods will load data from the folder that contains
        information for this date. Otherwise, it will create a folder that will
        contain information for the current date.

        Parameters
        ----------
        datapath: str
            An absolute path to a directory containing the date folders with paths
            "datapath/YYYY-MM-DD".
        col_demo_dist_path: str
            Absolute path where the demographic distribution from Colombia and its
            regions was saved using `covidcolombia.utils.colombia_demo_distribution`.
        cfr_korea_path: str
            Absolute path where the CFR for the Republic of Korea was saved using
            `covidcolombia.utils.korea_cfr`.
        region: str or None, optional
            Name of the region to be handled. If `"Colombia"` or `None`, the whole
            dataset is loaded.
        date: str or None, optional
            If it's a string, the format must be "YYYY-MM-DD".It refers to the name
            of a folder that contains information about a specfic date.
        delay_distribution: array-like or None, optional
            If it's an array, it should contain the delay distribution from onset
            to death. This can be 1D or 2D if many of these delay distributions
            are given when using probabilistic computing.
        """

        if date is None:
            date = str(datetime.date.today())

        if region == "Colombia":
            self.region = None
        else:
            self.region = region

        self.depto = None

        self.datapath = datapath
        self.col_demo_dist_path = col_demo_dist_path
        self.cfr_korea_path = cfr_korea_path
        self.date = date
        self.reports = {
            agerange2string(rango): Report(self.datapath, self.date, self.region, rango)
            for rango in AGE_RANGES
        }
        self.delay_distribution = delay_distribution

    def load_col_demo_dist(self):
        """
        Loads the demographic distribution from Colombia and its regions.

        Returns
        -------
        pd.DataFrame
            Dataframe containing the age distribution in predefined age ranges
            for Colombia and its regions.

        Notes
        -----
        Region can be a district or a municipality. Therefore, the database is
        loaded because when it gets loaded, the depto variable also gets written.
        """
        if self.region is None:
            region = "Colombia"
        else:
            self.reports[agerange2string(AGE_RANGES[-1])].load_cases_database()
            region = self.reports[agerange2string(AGE_RANGES[-1])].depto
        df = pd.read_csv(self.col_demo_dist_path)
        mask = df.departamento == region
        if mask.any():
            df = df[mask]
        else:
            raise ValueError(
                f"District {region} for self.region={self.region} is not saved at {self.col_demo_dist_path}. Check the DANE files manually."
            )
        df = df.set_index("rango")
        return df

    def load_cfr_korea(self):
        """
        Loads the CFR for the Republic of Korea.

        Returns
        -------
        pd.DataFrame
            Dataframe containing the CFR for different predefined age ranges for
            the Republic of Korea.
        """
        return pd.read_csv(self.cfr_korea_path).set_index("rango")

    def compute_reporting_percentage(
        self,
        day,
        truncation_type="fecha reporte web",
        truncation_day=None,
        mode="ignorant",
        dist='kde',
    ):
        """
        Computes the reporting percentage at a given `day` for all age ranges of a
        region. Uses Koopman errors to estimate the confidence intervals of this
        reporting percentage estimation.

        Parameters
        ----------
        day: str or datetime64
            Day in which to calculate the reporting percentage. If its a string,
            the format is "YYYY-MM-DD".
        truncation_type: str
            Can be 'fecha reporte web' to truncate given the date where cases
            are reported to INS, or 'FIS_all' to truncate given the onset symptoms
            date.
        truncation_day: datetime.datetime or None, optional
            A day where the cases database is truncated.
        mode: str, optional
            Can be either 'ignorant','wise' or 'prophet'. If 'ignorant',
            it will truncate the database at every day of the evolution
            range. If 'wise', it will not truncate. If 'prophet', truncates
            like 'ignorant', but predicts future cases to correct incidence.
        dist: str, optional
            A distribution to pass to `Report.fit_distribution`

        Returns
        -------
        pd.DataFrame
            A dataframe with columns 'rango' for age ranges, 'urep' for computed
            reporting percentages, 'urep_inf' and 'urep_sup' for inferior and
            superior bounds of reporting percentage, 'blCFR' `blCFR_inf` and
            `blCFR_sup` for the baseline CFR, 'cCFR', 'cCFR_inf' and 'cCFR_sup'
            for the corrected CFR, 'deaths' for the number of deaths, 'cases'
            for the number of cases and 'nCFR' for the naïve CFR.

        Notes
        -----
        If the corrected CFRs cannot be computed, then reporting percentages
        are returned as a state of complete ignorance, from 0% to 100%.

        Also, we assume that there is a very large population when computing
        the reporting percentage for a whole region, so that the Binomial
        variable defining the region's cCFR is approximated by a Normal
        distribution. Even though we could use the Delta method for computing
        the reporting percentage, we assume this large population to keep using
        the Koopman method.
        """

        col_demo_dist = self.load_col_demo_dist()
        cfr_korea = self.load_cfr_korea()
        range_names = [agerange2string(r) for r in AGE_RANGES]
        if self.delay_distribution is None:
            delay_distribution = self.reports[range_names[-1]].fit_distribution(
                dist=dist, kwargs={"samples": 1000}
            )
            if delay_distribution.ndim  == 2:
                delay_distribution = np.mean(delay_distribution, axis=0)
            self.delay_distribution = delay_distribution

        cCFRs = {}  # save cCFR tuples
        for rango in range_names:
            report = self.reports[rango]
            daily_incidence = report.get_daily_incidence(
                truncation_day, truncation_type
            ).incidence.to_numpy()
            cCFRs[rango] = list(
                report.correct_CFR(
                    day,
                    self.delay_distribution,
                    daily_incidence,
                    truncation_type,
                    truncation_day,
                    mode=mode,
                )
            )
        # Correct cCFR for the case of all ages
        cCFRs[range_names[-1]][0] = sum(
            [cCFRs[r][0] * col_demo_dist.loc[r].dist / 100 for r in range_names[:-1]]
        )
        cCFRs[range_names[-1]][1] = sum(
            [cCFRs[r][1] * col_demo_dist.loc[r].dist / 100 for r in range_names[:-1]]
        )
        cCFRs[range_names[-1]][2] = sum(
            [cCFRs[r][2] * col_demo_dist.loc[r].dist / 100 for r in range_names[:-1]]
        )
        reporting_percentage = {
            "rango": [],
            "urep": [],
            "urep_inf": [],
            "urep_sup": [],
            "blCFR": [],
            "blCFR_inf": [],
            "blCFR_sup": [],
            "cCFR": [],
            "cCFR_inf": [],
            "cCFR_sup": [],
            "deaths": [],
            "cases": [],
            "nCFR": [],
        }
        baseline_cfrs = {}
        baseline_cfrs_inf = {}
        baseline_cfrs_sup = {}
        # Compute underreporting for fixed age ranges:
        for rango in range_names[:-1]:
            row_kor = cfr_korea.loc[rango]
            baseline_cases = row_kor.casos
            baseline_deaths = row_kor.muertos
            baseline_cfrs[rango] = row_kor.cfr
            baseline_cfrs_inf[rango] = row_kor.inf
            baseline_cfrs_sup[rango] = row_kor.sup
            region_rango_cfr = cCFRs[rango][0]
            region_rango_deaths = cCFRs[rango][3]
            region_rango_cases_true = cCFRs[rango][4]
            nCFR_region_rango = region_rango_deaths / region_rango_cases_true
            # Check if cCFR could be computed:
            if region_rango_cfr == region_rango_cfr:
                if region_rango_cfr > 0:
                    region_rango_cases = round(
                        region_rango_deaths / (region_rango_cfr / 100)
                    )
                    prep, inf, sup = X_over_Y_koopman(
                        baseline_cases,
                        region_rango_cases,
                        baseline_deaths,
                        region_rango_deaths,
                    )
                else:
                    prep, inf, sup = 100, 100, 100
            else:
                # WARNING: We assume that we are completely ignorant about the
                # reporting percentage
                prep, inf, sup = 50, 0, 100
            reporting_percentage["rango"].append(rango)
            reporting_percentage["urep"].append(prep)
            reporting_percentage["urep_sup"].append(sup)
            reporting_percentage["urep_inf"].append(inf)
            reporting_percentage["blCFR"].append(row_kor.cfr)
            reporting_percentage["blCFR_inf"].append(row_kor.inf)
            reporting_percentage["blCFR_sup"].append(row_kor.sup)
            reporting_percentage["cCFR"].append(region_rango_cfr)
            reporting_percentage["cCFR_inf"].append(cCFRs[rango][1])
            reporting_percentage["cCFR_sup"].append(cCFRs[rango][2])
            reporting_percentage["deaths"].append(region_rango_deaths)
            reporting_percentage["cases"].append(region_rango_cases_true)
            reporting_percentage["nCFR"].append(nCFR_region_rango)

        # Compute underreporting for the whole age range:
        rango = range_names[-1]
        baseline_cfr = sum(
            [
                cfr * col_demo_dist.loc[r].dist / 100
                for (r, cfr) in baseline_cfrs.items()
            ]
        )
        baseline_cfr_inf = sum(
            [
                cfr * col_demo_dist.loc[r].dist / 100
                for (r, cfr) in baseline_cfrs_inf.items()
            ]
        )
        baseline_cfr_sup = sum(
            [
                cfr * col_demo_dist.loc[r].dist / 100
                for (r, cfr) in baseline_cfrs_sup.items()
            ]
        )
        # Consider the sample size to be very large
        # (the Delta-method can alternatively be used)
        baseline_cases = int(1e7)
        baseline_deaths = baseline_cases * baseline_cfr / 100
        region_cfr = cCFRs[rango][0]
        region_deaths = cCFRs[rango][3]
        region_cases_true = cCFRs[rango][4]
        nCFR_region = region_deaths / region_cases_true
        # Check if cCFR could be computed:
        if region_cfr == region_cfr:
            if region_cfr > 0:
                region_cases = round(region_deaths / (region_cfr / 100))
                prep, inf, sup = X_over_Y_koopman(
                    baseline_cases, region_cases, baseline_deaths, region_deaths
                )
            else:
                prep, inf, sup = 100, 100, 100
        else:
            # WARNING: We assume that we are completely ignorant about the
            # reporting percentage
            prep, inf, sup = 50, 0, 100
        reporting_percentage["rango"].append(rango)
        reporting_percentage["urep"].append(prep)
        reporting_percentage["urep_sup"].append(sup)
        reporting_percentage["urep_inf"].append(inf)
        reporting_percentage["blCFR"].append(baseline_cfr)
        reporting_percentage["blCFR_inf"].append(baseline_cfr_inf)
        reporting_percentage["blCFR_sup"].append(baseline_cfr_sup)
        reporting_percentage["cCFR"].append(region_cfr)
        reporting_percentage["cCFR_inf"].append(cCFRs[rango][1])
        reporting_percentage["cCFR_sup"].append(cCFRs[rango][2])
        reporting_percentage["deaths"].append(region_deaths)
        reporting_percentage["cases"].append(region_cases_true)
        reporting_percentage["nCFR"].append(nCFR_region)

        return pd.DataFrame(reporting_percentage)

    def reporting_percentage_evolution(
        self, initial_day, final_day, timestep=1, mode="ignorant", min_deaths=None, dist='kde',
    ):
        """
        Computes the evolution of reporting percentage from an `initial_day`
        to a `final_day`. There are different modes for doing this:

        - ignorant `mode`. To compute the rep. perc. at each day, data is
        truncated to that date.
        - wise `mode`. Data is not truncated, and all information from today's
        database is used.
        - prophet `mode`. Adopts the ignorant framework, but tries to predict
        the incidence from the coming days.

        Parameters
        ----------
        initial_day: str
            A date formated as a string: "YYYY-MM-DD".
        final_day: str
            A date formated as a string: "YYYY-MM-DD".
        timestep: int
            A timestep in days to report the evolution.
        mode: str, optional
            Can be either 'ignorant','wise' or 'prophet'. If 'ignorant',
            it will truncate the database at every day of the evolution
            range. If 'wise', it will not truncate. If 'prophet', truncates
            like 'ignorant', but predicts future cases to correct incidence.
        min_deaths: int or None, optional
            A number indicating the minimum number of deaths at the
            `initial_day`. If the number of deaths by the `initial_day`
            does not match `min_deaths`, then `initial_day` is overriden.
        dist: str, optional
            A distribution to pass to `Report.fit_distribution`
        Returns
        -------
        pd.DataFrame or None
            The columns are 'day' for every day between `initial_day` to the
            `final_day`, 'rango' for age ranges, 'urep' for computed reporting
            percentages, 'urep_inf' and 'urep_sup' for inferior and superior
            bounds of reporting percentage, 'blCFR' for the baseline
            CFR, 'cCFR', 'cCFR_inf' and 'cCFR_sup' for the corrected CFR,
            'deaths' for the number of deaths, 'cases' for the number of cases
            and 'nCFR' for the naïve CFR. If `initial_day` is after `final_day`,
            None is returned instead.

        """
        # TODO: move min_deaths to compute_reporting_percentage. Make sure that there are a minimum of deaths
        # at each age range, if possible!
        # TODO: also make sure that final_day - initial_day is greater than a minimum of days
        if isinstance(min_deaths, int):
            deaths_series = self.reports[
                agerange2string(AGE_RANGES[-1])
            ].load_cases_database(cols=["Fecha de muerte"])["Fecha de muerte"]
            cumm_deaths = deaths_series.value_counts().sort_index().cumsum()
            more_than_min_deaths = cumm_deaths >= min_deaths
            if more_than_min_deaths.any():
                initial_day = more_than_min_deaths.idxmax()
            else:
                return None
        if pd.to_datetime(initial_day) > pd.to_datetime(final_day):
            return None
        date_range = pd.date_range(initial_day, final_day, freq=f"{timestep}D")
        rep_percentages = []

        for day in date_range:
            if mode == "ignorant" or mode == "prophet":
                truncation_day = day
            elif mode == "wise":
                truncation_day = None
            else:
                raise ValueError(
                    f"mode {mode} is not supported. Try 'ignorant', 'wise' or 'prophet'."
                )
            rep_perc = self.compute_reporting_percentage(
                day,
                truncation_type="fecha reporte web",
                truncation_day=truncation_day,
                mode=mode,
                dist=dist,
            )
            rep_perc.loc[:, "day"] = day
            rep_percentages.append(rep_perc)

        return pd.concat(rep_percentages)

    def get_curves(self, day, earliest_day=None, mode="ignorant", dist='kde'):
        """
        Computes the following curves:

        - Daily deaths
        - Incidence
        - Prevalence
        - Corrected cummulative cases

        where the corrected cummulative cases curve accounts for underreporting.

        Parameters
        ----------
        day: str or datetime64
            A date formated as a string: "YYYY-MM-DD" or a datetime64.
            Curves are computed up to that given day.
        earliest_day: str or datetime64, optional
            A date formated as a string: "YYYY-MM-DD" or a datetime64.
            Curves are computed from this day (if None, earliest date
            is automatically taken).
        dist: str, optional
            A distribution to pass to `Report.fit_distribution`

        Returns
        -------
        pandas.DataFrame
            A pandas dataframe containing the information for some curves.
            It has the fields `date` and `rango`. By filtering a single value
            of `rango` one ends with several time series for that `rango`
            value. `date` indexes the date of the death incidence
            (death_incidence), case incidence (case_incidence), case
            prevalence (case_prevalence), and corrected cummulative cases
            (corrected_cummulative_cases) and its inferior and superior bounds
            (corrected_cummulative_cases_inf, and corrected_cummulative_cases_sup)

        Notes
        -----
        For SIR-like models the corrected incidence can be used to fit
        a curve. What curve? S(t) - S(t-1) gives the number of people
        infected in the last day. This happens because susceptible
        population flows towards the infected compartment only.
        Therefore, the curve S(t) - S(t-1) is the incidence curve.
        The incidence curve, corrected to account for underreporting
        can be computed from the resulting DataFrame of this method,
        say `df`, in the following manner. 1) First filter a single
        age range from `df`. 2) Then `df.corrected_cummulative_cases.diff()`
        gives the incidence curve.
        """
        if earliest_day is None:
            largest_report = self.reports[agerange2string(AGE_RANGES[-1])]
            largest_db = largest_report.load_cases_database(fis_all=True)
            earliest_day = largest_db.FIS_all.min()
        date_range = pd.date_range(earliest_day, day)
        dates = []
        age_range = []
        death_incidence = []
        case_incidence = []
        case_prevalence = []
        corrected_cummulative_cases = []
        corrected_cummulative_cases_inf = []
        corrected_cummulative_cases_sup = []
        for date in date_range:
            rep_df = self.compute_reporting_percentage(date, mode=mode, dist=dist)
            rep_df = rep_df.set_index("rango")
            for rango, report in self.reports.items():
                age_range.append(rango)
                num_deaths, num_recovered, num_infected = report.get_SIR_populations(
                    date, mode="incidence"
                )
                death_incidence.append(num_deaths)
                case_incidence.append(num_infected)
                dates.append(date)
                num_deaths, num_recovered, num_infected = report.get_SIR_populations(
                    date, mode="cummulative"
                )
                case_prevalence.append(num_infected - num_deaths - num_recovered)
                urep = rep_df.loc[rango]["urep"] / 100
                urep_inf = rep_df.loc[rango]["urep_inf"] / 100
                urep_sup = rep_df.loc[rango]["urep_sup"] / 100
                corrected_cummulative_cases.append(num_infected / urep)
                corrected_cummulative_cases_sup.append(num_infected / urep_inf)
                corrected_cummulative_cases_inf.append(num_infected / urep_sup)

        return pd.DataFrame(
            dict(
                date=dates,
                rango=age_range,
                death_incidence=death_incidence,
                case_incidence=case_incidence,
                case_prevalence=case_prevalence,
                corrected_cummulative_cases=corrected_cummulative_cases,
                corrected_cummulative_cases_inf=corrected_cummulative_cases_inf,
                corrected_cummulative_cases_sup=corrected_cummulative_cases_sup,
            )
        )
