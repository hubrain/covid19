from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="covidcolombia",
    packages=find_packages(include=["covidcolombia"]),
    version='0.1.4',
    description="Library for underreporting analysis of COVID-19 in Colombia",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Nicolás Parra, Juan Flórez, Leonel Ardila, Vladimir Vargas and Carlos Viviescas",
    author_email="{nparraa, jsflorezj, lfardilap, vvargasc}@analist.ai, clviviescasr@unal.edu.co",
    license="GNUv3",
    install_requires=[
        'pandas','numpy','datetime','scipy','pymc3','wget','statsmodels','matplotlib',
    ],
    python_requires='>=3.6'
)
