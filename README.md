[![pipeline status](https://gitlab.com/analist-ai/covid19/badges/master/pipeline.svg)](https://gitlab.com/analist-ai/covid19/-/commits/master)  [![coverage report](https://gitlab.com/analist-ai/covid19/badges/master/coverage.svg)](https://gitlab.com/analist-ai/covid19/-/commits/master)
[![Paper](https://img.shields.io/badge/Paper-medRxiv-blue)](https://www.medrxiv.org/content/10.1101/2020.12.30.20249052v1)

# Covid19 Pandemic in Colombia

This repository contains references and code about estimatint the real size of the pandemic in Colombia. It is well-known that governments are unable to completely sample the population. Therefore, the reported figures by Instituto Nacional de Salud are an **underestimation** of the real quantity of people infected with SARS-CoV-2.

Our library provides an easy way of computing the reporting percentage, which is the percentage of the true cases that the INS is able to report through COVID-19 testing. Therefore, our library is also able to **estimate the true number of positive SARS-CoV-2 cases in Colombia and any of its regions**.

# Installation

You can clone this repository by opening a terminal window and running `git clone https://gitlab.com/hubrain/covid19.git `. Then, you can install the library via `pip install git+https://gitlab.com/hubrain/covid19.git `. That's it!

If after installation you cannot import `pymc3` (because it depends on Theano), we recommend that you install the developing version of Python through `sudo apt-get install python3.8-dev` in Linux, or in Mac it should be `brew update && brew rm python3 && brew install python3`.

# Getting Started

First, you will need to create a file (`covidcolombia` does this for you) that contains the demographic distribution of Colombia and its regions with

```python
from covidcolombia.utils import colombia_demo_distribution
colombia_demo_distribution(dane_distribution_folder, path_to_file, perc_threshold)
```

See the docs with `help(colombia_demo_distribution)` for more information about this function. But basically, you have created some file at `path_to_file`. Then, another file containing the CFR of the Republic of Korea is needed:

```python
from covidcolombia.utils import korea_cfr
korea_cfr(path_to_file)
```

where you have created some file at `path_to_file` (see documentation with `help(korea_cfr)`).

After that, you can check the reporting percentage of a region at a given day (say today) simply by doing the following

```python
from covidcolombia import Region
import datetime
import pandas as pd

today = datetime.date.today()
valledelcauca = Region(datapath, col_demo_dist_path, cfr_korea_path, region="Valle del Cauca", date=str(today))
print(valledelcauca.compute_reporting_percentage(pd.to_datetime(today)))
```

**Check our examples folder!**

# Structure of the library

The main class of the library is `Region`. It generates `Report` objects to compute reporting percentages. As of version 0.1.4, our library tries to be memory efficient. However, it might be slow. Version 0.2.0 will attack this issue.

The databases and library are linked together as follows:

```mermaid
graph LR
    initreg(Initialise a Region) --automatic-->initrep1(Initialise Report for<br>first age range)
    initreg --automatic-->initrep2(Initialise Report for<br>second age range)
    initreg --automatic-->initrep(Initialise Report for<br>i-th age range)
    initreg --automatic-->initrepw(Initialise Report for<br>whole age range)
    initrep --automatic-->downloadins[[Download cases<br>and tests from<br>datos abiertos]]
    ins[(INS Database)]-->downloadins
    downloadins-->local[(Local Database)]
    coldemo[(DANE demographic distribution)]-->local
    initrep --> underrep(Compute reporting percentage)
    local --> underrep
```

# For Developers

Install the virtual environment with `pipenv`. This sets a standard environment to reproduce all the notebooks and code.

Whenever you make a new function, please add a test for it using the `pytest` convention.

# Current lines of work

We argue that every epidemiological model fitted to real data is wrong if the fit does not take into account both underreporting and delay of reporting. We are looking to provide a "*corrected*" dataset, which contains synthetic people accounting for these phenomena. With this synthetic dataset it could be possible to fit a more realistic model, probably yielding much better predictions. Some issues that we are facing are:

- How do we know the current amount of active cases (case prevalence)? At the moment we know the reporting percentage. Thus, we know the *total* amount of cases, but not the amount of active cases.
- We are discussing the validity of a forecasting method used to estimate the number of future cases reported at each day by the INS. This means that, for instance, tomorrow 100 cases will be discovered, but their onset date is in the past. We are currently "able" to correct this by predicting future case discovery by INS. But we know that these future cases are an underestimation of the real amount of cases. How do we find the "true" amount of cases incident in one day? If we do this, we can apply the onset-to-recovery distribution to those incident cases (corrected by including future discovered cases as well as by underestimation) in order to keep track of the "real" number of recovered cases. Thus, we could easily estimate the amount of active cases.

If we are able to solve those problems, we can confidently provide the so-called *corrected* dataset for epidemiological simulation.
